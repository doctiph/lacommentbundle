<?php

namespace La\CommentBundle\Cache;

interface CacheManagerInterface
{

    public function get($arrayKeys, $arrayTags, callable $callback, $ttl = null);

    public function deleteByTags($arrayTags);

    public function deleteByKeys($arrayKeys);

}
