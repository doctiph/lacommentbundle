<?php

namespace La\CommentBundle\Cache;

use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class CacheManager implements CacheManagerInterface
{

    public function get($arrayKeys, $arrayTags, callable $callback, $ttl = null)
    {
        return $callback->__invoke();
    }

    public function deleteByTags($arrayTags)
    {
        return true;
    }

    public function deleteByKeys($arrayKeys)
    {
        return true;
    }

    public function getArrayKeysForThreadCache($id)
    {
        $result = [];
        if (is_array($id)) {
            foreach ($id as $i) {
                $result[] = sprintf('thread_%s', $i);
            }
        } else {
            $result[] = sprintf('thread_%s', $id);
        }
        return $result;
    }

    public function getArrayKeysForCommentCache($id)
    {
        $result = [];
        if (is_array($id)) {
            foreach ($id as $i) {
                $result[] = sprintf('comment_%s', $i);;
            }
        } else {
            $result[] = sprintf('comment_%s', $id);
        }
        return $result;
    }

}
