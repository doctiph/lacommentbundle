<?php

namespace La\CommentBundle\Cache;

class CacheMemcachedManager extends CacheManager
{
    protected $memcached;

    const TAG_LIST_KEY_PREFIX = 'la_memcache_key_list';
    const TAG_LIST_KEY_EXPIRATION = 86400;
    const debug = false;

    public function __construct($memcached)
    {
        $this->memcached = $memcached;
    }

    /**
     * @param $arrayKeys
     * @param null $arrayTag
     * @param callable $callback
     * @param null $expiration
     * @return mixed|null
     */
    public function get($arrayKeys, $arrayTag = null, callable  $callback = null, $expiration = null)
    {
        $key = $this->generateKey($arrayKeys);
        $value = $this->memcached->getInstance()->get($key);
        $this->debug(sprintf('Get key %s<br />', $key));
        // SI la clef n'existe pas, on ne poyurra plus décacher par tag : donc on recache
        if (!$this->tagsHaveKey($key) || $this->memcached->getInstance()->getResultCode() === \Memcached::RES_NOTFOUND) {

            if (!is_null($callback)) {
                $value = $callback->__invoke();
                if (null !== $arrayTag) {
                    $this->addKeyToTag($arrayTag, $key);
                }
                $this->memcached->getInstance()->delete($key);
                $this->memcached->getInstance()->set($key, $value, $expiration);
            } else {
                return null;
            }
        } else if ($this->memcached->getInstance()->getResultCode() !== \Memcached::RES_SUCCESS) {
            return null;
        } else {
            $this->debug(sprintf('Key %s found', $key));
        }
        return $value;
    }


    /**
     * @param $keys
     * @return bool
     */
    public function deleteByKeys($keys)
    {
        if (is_array($keys)) {
            foreach ($keys as $key) {
                $this->delete($key);
            }
        } else {
            $this->delete($keys);
        }
        return true;
    }

    /**
     * @param $tags
     * @return bool
     */
    public function deleteByTags($tags)
    {
        if (is_array($tags)) {
            foreach ($tags as $tag) {
                $this->deleteByTag($tag);
            }
        } else {
            $this->deleteByTag($tags);
        }
        return true;
    }


    /**
     * Generate cache key from array string
     *
     * @param $keys
     * @return string
     */
    protected function generateKey($keys)
    {
        if (!is_array($keys)) {
            return sprintf('%s%s', $this->memcached->getPersistentId(), $keys);
        }
        if (count($keys) === 1) {
            return sprintf('%s%s', $this->memcached->getPersistentId(), $keys[0]);
        }
        $key = '';
        foreach ($keys as $i => $k) {
            if ($i) {
                $key .= '__' . $k;
            } else {
                $key .= $k;
            }
        }
        $k = sprintf('%s%s', $this->memcached->getPersistentId(), $key);
        if (strlen($k > 255)) {
            $k = sprintf('%s%s', $this->memcached->getPersistentId(), md5($key));
        }
        return $k;
    }

    protected function delete($key, $expiration = null)
    {
        $this->memcached->getInstance()->delete($key, $expiration);
        $this->debug(sprintf('deleteing key %s', $key));
        if ($this->memcached->getInstance()->getResultCode() == \Memcached::RES_SUCCESS) {
            return true;
        }
        return false;
    }


    protected function deleteByTag($tag)
    {
        $tags = $this->getTags();
        if (isset($tags[$tag])) {
            foreach ($tags[$tag] as $key) {
                $this->memcached->getInstance()->delete($key);
            }
        }
        return true;
    }

    protected function addKeyToTag($arrayTag, $key)
    {
        $tagList = $this->getTags();
        foreach ($arrayTag as $tag) {
            if (!isset($tagList[$tag])) {
                $tagList[$tag] = array();
            }
            if (!in_array($key, $tagList[$tag])) {
                $tagList[$tag][] = $key;
            }
        }
        $this->memcached->getInstance()->delete($this->getTagListKey());
        $this->memcached->getInstance()->set($this->getTagListKey(), $tagList, static::TAG_LIST_KEY_EXPIRATION);

    }

    protected function debug($message)
    {
        $separator = '<br />';
        if (static::debug) {
            echo $message . $separator;
        }
    }

    protected function getTagListKey()
    {
        return sprintf('%s%s', $this->memcached->getPersistentId(), static::TAG_LIST_KEY_PREFIX);
    }

    protected function tagsHaveKey($key)
    {
        $tags = $this->getTags();
        foreach ($tags as $k) {
            if ($key === $k) {
                return true;
            }
        }
        return false;
    }

    protected function getTags()
    {
        $tags = $this->memcached->getInstance()->get($this->getTagListKey());
        if ($this->memcached->getInstance()->getResultCode() === \Memcached::RES_NOTFOUND) {
            return [];
        }
        return $tags;
    }
}