<?php



namespace La\CommentBundle\Sorting;

use La\CommentBundle\Model\CommentInterface;

/**
 * Sorts comments by date order.
 *
 */
class DateSorting extends AbstractOrderSorting
{
    protected $property = 'createdAt';

    /**
     * Compares the comments creation date.
     *
     * @param  CommentInterface $a
     * @param  CommentInterface $b
     * @return -1|0|1           As expected for uasort()
     */
    protected function compare(CommentInterface $a, CommentInterface $b)
    {
        if ($a->getCreatedAt() === $b->getCreatedAt()) {
            return 0;
        }

        return $a->getCreatedAt() < $b->getCreatedAt() ? -1 : 1;
    }
}
