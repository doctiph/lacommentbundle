<?php



namespace La\CommentBundle\Sorting;

use La\CommentBundle\Model\CommentInterface;

/**
 * Sorts comments by vote order.
 *
 */
class VoteSorting extends AbstractOrderSorting
{
    /**
     * Compares the comments score.
     *
     * @param  CommentInterface $a
     * @param  CommentInterface $b
     * @return -1|0|1           As expected for uasort()
     */
    protected function compare(CommentInterface $a, CommentInterface $b)
    {
        if ($a->getScore() == $b->getScore()) {
            return 0;
        }

        return $a->getScore() < $b->getScore() ? -1 : 1;
    }
}
