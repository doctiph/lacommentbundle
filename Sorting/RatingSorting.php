<?php

namespace La\CommentBundle\Sorting;

use La\CommentBundle\Model\CommentInterface;

/**
 * Sorts comments by score.
 *
 */
class RatingSorting extends AbstractOrderSorting
{
    protected $property = 'rating';

    /**
     * Compares the comments rating of commented content.
     *
     * @param  CommentInterface $a
     * @param  CommentInterface $b
     * @return -1|0|1           As expected for uasort()
     */
    protected function compare(CommentInterface $a, CommentInterface $b)
    {
        if ($a->getRating() === $b->getRating()) {
            return 0;
        }

        return $a->getRating() < $b->getRating() ? -1 : 1;
    }
}
