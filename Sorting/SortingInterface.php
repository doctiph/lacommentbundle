<?php



namespace La\CommentBundle\Sorting;

use La\CommentBundle\Model\Tree;

/**
 * Interface to be implemented for adding additional Sorting strategies.
 *
 */
interface SortingInterface
{
    /**
     * Takes an array of Tree instances and sorts them.
     *
     * @param  array $tree
     * @return Tree
     */
    public function sort(array $tree);

    /**
     * Sorts a flat comment array.
     *
     * @param  array $comments
     * @return array
     */
    public function sortFlat(array $comments);

    public function getOrder();

    public function getProperty();
}
