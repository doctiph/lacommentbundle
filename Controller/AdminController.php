<?php

namespace La\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use La\AdminBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use La\AdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{

    const MAX_COMMENTS_PER_PAGE = 100;
    const MAX_COMMENTS_ALERTED_PER_PAGE = 100;

    protected $currentMenu = 'la_comment_admin.nav.comments.default';

    /**
     * @param Request $request
     * @return Response
     */
    public function pendingAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }
        $sorter = 'date_asc';
        $page = $this->get('request')->query->get('page', 1); /*page number*/
        $limit = static::MAX_COMMENTS_PER_PAGE; /*limit per page*/

        if ($request->query->has('sorter')) {
            if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                $sorter = $request->query->get('sorter');
            }
        }
        $manager = $this->container->get('la_comment.manager.moderation');
        $comments = $manager->findLastPendingComments(($page - 1) * $limit, $limit, $sorter);
        $causes = $manager->getModerationCauses();
        $count = $manager->countPendingComments();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            range(0, $count - 1),
            $page,
            $limit
        );
        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:pending.html.twig', array(
            "comments" => $comments,
            "causes" => $causes,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_comment_admin.nav.comments.moderate',

            'sorter' => $sorter,
            'pagination' => $pagination
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return bool
     */
    public function validateAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }
        try {
            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }
            $moderation = $this->container->get('la_comment.manager.moderation');
            $manager = $this->container->get('la_comment.manager.comment');


            $comment = $manager->findCommentById($id);
            $moderation->setModerated($comment, $moderation::MODERATION_OK, null, $request->getUser(), new \DateTime());

            $manager->saveComment($comment);

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }


    /**
     * @param Request $request
     * @param $id
     * @param $cause
     * @return bool
     */
    public function rejectAction(Request $request, $id, $cause)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        try {

            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }

            $moderation = $this->container->get('la_comment.manager.moderation');
            $manager = $this->container->get('la_comment.manager.comment');

            $comment = $manager->findCommentById($id);
            $moderation->setModerated($comment, $moderation::MODERATION_NOT_OK, urldecode($cause), $request->getUser(), new \DateTime());

            $manager->saveComment($comment);

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param $until
     * @param $message
     */
    public function banAction(Request $request, $id, $until, $message)
    {
        /** @TODO */
        die('coming soon');
        $this->container->get("la_comment.manager.moderation")->ban($id, $until, $message, $request->getUser());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function recentsAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        $sorter = 'date_desc';
        if ($request->query->has('sorter')) {
            if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                $sorter = $request->query->get('sorter');
            }
        }

        $page = $this->get('request')->query->get('page', 1); /*page number*/
        $limit = static::MAX_COMMENTS_PER_PAGE; /*limit per page*/

        $manager = $this->container->get('la_comment.manager.moderation');
        $comments = $manager->findRecentlyModeratedComments(($page - 1) * $limit, $limit, $sorter);
        $causes = $manager->getModerationCauses();
        $count = $manager->countModeratedComments();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            range(0, $count - 1),
            $page,
            $limit
        );

        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:recents.html.twig',
            array(
                "comments" => $comments,
                "causes" => $causes,
                "causes" => $causes,
                'current_menu' => $this->currentMenu,
                'current_sub_menu' => 'la_comment_admin.nav.comments.recents',
                'sorter' => $sorter,
                'pagination' => $pagination
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function refusedAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        $sorter = 'date_desc';
        if ($request->query->has('sorter')) {
            if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                $sorter = $request->query->get('sorter');
            }
        }

        $manager = $this->container->get('la_comment.manager.moderation');

        $causes = $manager->getModerationCauses();
        $page = $this->get('request')->query->get('page', 1); /*page number*/
        $limit = static::MAX_COMMENTS_PER_PAGE; /*limit per page*/
        $comments = $manager->findRefusedComments(($page - 1) * $limit, $limit, $sorter);
        $count = $manager->countRefusedComments();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            range(0, $count - 1),
            $page,
            $limit
        );

        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:refused.html.twig', array(
            "comments" => $comments,
            "causes" => $causes,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_comment_admin.nav.comments.refused',
            'sorter' => $sorter,
            'pagination' => $pagination
        ));

    }


    /**
     * @param Request $request
     * @return Response
     */
    public function commentAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        $manager = $this->container->get('la_comment.manager.moderation');
        $commentManager = $this->container->get('la_comment.manager.comment');

        $causes = $manager->getModerationCauses();
        $comment = $commentManager->findCommentById($id);

        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:comment.html.twig', array(
            "comments" => array($comment),
            "causes" => $causes,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_admin.nav.settings.users.search',
        ));

    }


    public function searchAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        $manager = $this->container->get('la_comment.manager.moderation');

        $sorter = 'date_desc';
        if ($request->query->has('sorter')) {
            if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                $sorter = $request->query->get('sorter');
            }
        }

        $form = $this->createFormBuilder()
            ->add('id', 'text', array('label' => 'ID', 'required' => false))
            ->add('title', 'text', array('label' => 'Titre', 'required' => false))
            ->add('comment', 'text', array('label' => 'Commentaire', 'required' => false))
            ->add('username', 'text', array('label' => 'Username', 'required' => false))
            ->add('userid', 'text', array('label' => 'UserId', 'required' => false))
            ->add('ip', 'text', array('label' => 'Ip', 'required' => false))
            ->add('state', 'choice', array(
                'choices' => array(
                    'nop' => 'Indéterminés',
                    'ok' => 'Validés',
                    'ko' => 'Refusés',
                ),
                'required' => false,
            ))
            ->add('moderation_cause', 'choice', array(
                'choices' => $manager->getModerationCauses(),
                'required' => false,
            ))
            ->add('search', 'submit', array('label' => 'Rechercher'))
            ->getForm();

        $form->handleRequest($request);
        $result = null;

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            $sorter = 'date_desc';
            $limit = static::MAX_COMMENTS_PER_PAGE; /*limit per page*/

            if (isset($data)) {
                $result = $manager->getCommentsFromCriteria($data, 0, $limit, $sorter);
            }
        }

        if (!is_null($result)) {
            $causes = $manager->getModerationCauses();

            if ($request->query->has('sorter')) {
                if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                    $sorter = $request->query->get('sorter');
                }
            }

            if (!empty($data['state'])) {
                $data['state'] = 'la_comment_admin.search.' . $data['state'];
            }
            return $this->get('templating')->renderResponse('LaCommentBundle:Admin:result.html.twig', array(
                "comments" => $result,
                "causes" => $causes,
                'current_menu' => $this->currentMenu,
                'current_sub_menu' => 'la_comment_admin.nav.comments.search',
                'data' => $data,
                'count' => $manager->getCountCommentsFromCriteria($data),
                'limit' => $limit
            ));
        }

        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:search.html.twig', array(
            'form' => $form->createView(),
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_comment_admin.nav.comments.search'
        ));
    }

    // STATS
    public function statsMonthlyAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->render('LaCommentBundle:Admin:Stats/monthly.html.twig', array(
            'layout' => "LaStatsBundle:Display:admin_layout.html.twig",
            'type' => 'comment',
            'current_menu' => 'la_comment_admin.nav.stats.default',
            'current_sub_menu' => 'la_comment_admin.nav.stats.comments_monthly',
        ));
    }

    // STATS
    public function statsYearlyAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->render('LaCommentBundle:Admin:Stats/yearly.html.twig', array(
            'layout' => "LaStatsBundle:Display:admin_layout.html.twig",
            'type' => 'comment',
            'current_menu' => 'la_comment_admin.nav.stats.default',
            'current_sub_menu' => 'la_comment_admin.nav.stats.comments_yearly',
        ));
    }

    // Alerts
    public function alertsAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }
        $alertManager = $this->container->get('la_comment.manager.alert.default');
        $manager = $this->container->get('la_comment.manager.moderation');

        $causes = $manager->getModerationCauses();
        $sorter = 'date_desc';
        $page = $this->get('request')->query->get('page', 1); /*page number*/
        $limit = static::MAX_COMMENTS_ALERTED_PER_PAGE; /*limit per page*/

        if ($request->query->has('sorter')) {
            if (in_array($request->query->get('sorter'), ['date_asc', 'date_desc'])) {
                $sorter = $request->query->get('sorter');
            }
        }

        $count = $alertManager->countAlerts();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            range(0, $count - 1),
            $page,
            $limit
        );

        $alerts = $alertManager->getAlerts(($page - 1) * $limit, $limit, $sorter);


        return $this->get('templating')->renderResponse('LaCommentBundle:Admin:alerts.html.twig', array(
            "causes" => $causes,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_comment_admin.nav.comments.alerts',
            'sorter' => $sorter,
            'pagination' => $pagination,
            'alerts' => $alerts
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function rejectAlertAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        try {

            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }

            $alertManager = $this->container->get('la_comment.manager.alert.default');
            $alert = $alertManager->findAlertById($id);
            $alertManager->rejectAndSaveAlert($alert, $this->get('security.context')->getToken()->getUser());

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    public function acceptAlertAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_COMMENTS_MODERATE')) {
            throw new AccessDeniedException();
        }

        try {

            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }

            $alertManager = $this->container->get('la_comment.manager.alert.default');
            $alert = $alertManager->findAlertById($id);
            $alertManager->solveAndSaveAlert($alert, $this->get('security.context')->getToken()->getUser());

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

//
}