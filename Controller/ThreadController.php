<?php



namespace La\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use FOS\RestBundle\View\ViewHandlerInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;
use La\ApiBundle\Controller\AbstractApiController;
use La\CommentBundle\Model\Comment;
use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\Thread;
use La\CommentBundle\Model\ThreadInterface;
use La\CommentBundle\Exception as ApiException;

/**
 * Restful controller for the Threads.
 */
class ThreadController extends AbstractApiController
{
    const VIEW_FLAT = 'flat';
    const VIEW_TREE = 'tree';

    const KEY = 'comment';
    const DEFAULT_RSS_ITEMS_COUNT = 20;
    const DEFAULT_TTL = 300;

    /**
     * Gets the thread for a given id.
     *
     * @param string $id
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return View
     */
    public function getThreadAction($id)
    {
        try {
            $thread = $this->getThreadFromCache($id);

            if (null === $thread) {
                throw new NotFoundHttpException(sprintf("Thread with id '%s' could not be found.", $id));
            }

            $view = View::create()
                ->setData(array('thread' => $thread));

            $this->addModerationCache($view);

            return $this->getViewHandler()->handle($view);
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * Gets the threads for the specified ids.
     *
     * @param Request $request
     *
     * @return View
     */
    public function getThreadsActions(Request $request)
    {
        $ids = $request->query->get('ids');
        $manager = $this->container->get('la_comment.manager.thread');
        // cache
        $cacheManager = $this->getCacheManager();
        $threads = $cacheManager->get(
            $cacheManager->getArrayKeysForThreadCache($ids),
            $cacheManager->getArrayKeysForThreadCache($ids),
            function () use ($manager, $ids) {
                return $manager->findThreadsBy(array('id' => $ids));
            },
            $this->container->getParameter('la_comment.cache.thread_cache_ttl')
        );
        // end

        $view = View::create()
            ->setData(array('threads' => $threads));

        $this->addModerationCache($view);

        return $this->getViewHandler()->handle($view);
    }

    /**
     * Gets the moderated threads during the last $since seconds
     *
     * @param Request $request
     * @internal param int $since since (in seconds)
     *
     * @return View
     */
    public function moderatedThreadsAction(Request $request)
    {
        try {
            // $since is in second. Default 600s, 10mn
            $since = !is_null($request->get('since')) ? $request->get('since') : 600;
            if(!is_numeric($since)){
                throw new InvalidParameterException('Invalid since parameter (expected seconds, got :"'.$since.'")');
            }
            $threads = $this->container->get('la_comment.manager.comment')->findModeratedThreads((int) $since);

            $view = View::create()->setData(array('threads' => $threads));
            $this->addModerationCache($view);
            return $this->getViewHandler()->handle($view);
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * Creates a new Thread from the submitted data.
     *
     * @param Request $request The current request
     *
     * @return View
     */
    public function postThreadsAction(Request $request)
    {
        try {
            $this->checkSecurity();

            $threadManager = $this->container->get('la_comment.manager.thread');
            $thread = $threadManager->createThread();
            $form = $this->container->get('la_comment.form_factory.thread')->createForm(array('NewThread'));
            $form->setData($thread);

            if ('POST' === $request->getMethod()) {
                $errors = $this->getErrors($form, $request, array('NewThread'));

                if ($errors !== false) {
                    return $this->sendError(array('form' => $errors));
                }
                if ($form->isValid()) {
                    if (null !== $threadManager->findThreadById($thread->getId())) {
                        throw new ApiException\CreateThreadDuplicateException();
                    }
                    if (is_null($thread->getType())) {
                        $thread->setType($thread::THREAD_TYPE);
                    }
                    // Add the thread
                    $threadManager->saveThread($thread);

                    return $this->getViewHandler()->handle($this->onFormSuccess($form, Codes::HTTP_CREATED));
                } else {
                    die('invalid form');
                }
            }

        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * Switch a thread : commentable or not.
     *
     * @param Request $request Currenty request
     * @param mixed $id Thread id
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return View
     */
    public function patchThreadsCommentableAction(Request $request, $id)
    {
        $manager = $this->container->get('la_comment.manager.thread');
        $thread = $manager->findThreadById($id);

        if (null === $thread) {
            throw new NotFoundHttpException(sprintf("Thread with id '%s' could not be found.", $id));
        }

        $form = $this->container->get('la_comment.form_factory.commentable_thread')->createForm();

        $form->setData($thread);

        if ('PATCH' === $request->getMethod()) {
            $form->submit($request);

            if ($form->isValid()) {
                $manager->saveThread($thread);

                return $this->getViewHandler()->handle($this->onFormSuccess($form));
            }
        }

        return $this->getViewHandler()->handle($this->onOpenThreadError($form));
    }

    /**
     * Switch the thread : Enabled or not
     *
     * @param Request $request Currenty request
     * @param mixed $id Thread id
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return View
     */
    public function patchThreadsEnabledAction(Request $request, $id)
    {
        $manager = $this->container->get('la_comment.manager.thread');
        $thread = $manager->findThreadById($id);

        if (null === $thread) {
            throw new NotFoundHttpException(sprintf("Thread with id '%s' could not be found.", $id));
        }

        $form = $this->container->get('la_comment.form_factory.enabled_thread')->createForm();

        $form->setData($thread);

        if ('PATCH' === $request->getMethod()) {
            $form->submit($request);

            if ($form->isValid()) {
                $manager->saveThread($thread);

                return $this->getViewHandler()->handle($this->onFormSuccess($form));
            }
        }

        return $this->getViewHandler()->handle($this->onOpenThreadError($form));
    }

    /**
     * Get last comments posted
     *
     * @deprecated
     * @param Request $request
     * @return Response
     */
    public function getThreadsLastCommentsAction(Request $request)
    {
        die('deprecated');
        $state = $request->query->get('state');
        switch ($state) {
            case 'pending' :
                $state = Comment::STATE_PENDING;
                $title = "Last pending comments";
                break;
            case 'deleted' :
                $state = Comment::STATE_DELETED;
                $title = "Last deleted comments";
                break;
            case 'spam' :
                $state = Comment::STATE_SPAM;
                $title = "Last comments flagged as spam";
                break;
            default :
                $title = "Last comments";
                if (!is_numeric($state)) {
                    $state = null;
                }
                break;
        }

        $count = $request->query->get('count');
        if (is_null($count)) {
            $count = self::DEFAULT_RSS_ITEMS_COUNT;
        }

        /** @var View $view */
        $view = View::create()
            ->setData(array(
                'title' => $title,
                'requested' => new \DateTime(),
                'comments' => $this->container->get('la_comment.manager.comment')->findLastComments(0, $count, $state),
            ))
            ->setFormat('rss');

        $this->get('fos_rest.view_handler')->registerHandler(
            'rss',
            function (ViewHandlerInterface $handler, View $view, $request) {
                $view->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'lastcomments_xml_feed'));

                return new Response($handler->renderTemplate($view, 'rss'), Codes::HTTP_OK, $view->getHeaders());
            }
        );

        return $this->getViewHandler()->handle($view);
    }

    /**
     * Gets last active threads
     *
     * @deprecated
     * @param Request $request
     * @return Response
     */
    public function getThreadsLastActiveAction(Request $request)
    {
        die('deprecated');

        $count = $request->query->get('count');
        if (is_null($count)) {
            $count = self::DEFAULT_RSS_ITEMS_COUNT;
        }

        /** @var View $view */
        $view = View::create()
            ->setData(array(
                'title' => 'Last threads',
                'requested' => new \DateTime(),
                'threads' => $this->container->get('la_comment.manager.thread')->findLastActiveThreads($count),
            ))
            ->setFormat('rss');

        $this->get('fos_rest.view_handler')->registerHandler(
            'rss',
            function (ViewHandlerInterface $handler, View $view, $request) {
                $view->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'lastthreads_xml_feed'));

                return new Response($handler->renderTemplate($view, 'rss'), Codes::HTTP_OK, $view->getHeaders());
            }
        );

        return $this->getViewHandler()->handle($view);

    }

    /**
     * Get a comment of a thread.
     *
     * @param string $id Id of the thread
     * @param mixed $commentId Id of the comment
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return View
     */
    public function getThreadCommentAction($id, $commentId)
    {
        $threadManager = $this->container->get('la_comment.manager.thread');
        $commentManager = $this->container->get('la_comment.manager.comment');

        $thread = $threadManager->findThreadById($id);
        $comment = $commentManager->findCommentById($commentId);

        if (null === $thread || null === $comment || $comment->getThread() !== $thread) {
            throw new NotFoundHttpException(sprintf("No comment with id '%s' found for thread with id '%s'", $commentId, $id));
        }


        $view = View::create()
            ->setData(array('comment' => $comment, 'thread' => $thread))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment'));

        $this->addModerationCache($view);

        return $this->getViewHandler()->handle($view);
    }

    /**
     * Edit the moderation state
     *
     * @param Request $request
     * @param $id
     * @param $commentId
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Exception
     */
    public function patchThreadCommentStateAction(Request $request, $id, $commentId)
    {
        $manager = $this->container->get('la_comment.manager.comment');
        $thread = $this->container->get('la_comment.manager.thread')->findThreadById($id);
        /* @var CommentInterface $comment */
        $comment = $manager->findCommentById($commentId);

        if (is_null($request->get('userid'))) {
            throw new NotFoundHttpException("No userid found in request.");
        }
        if ($comment->getUserId() != $request->get('userid')) {
            throw new NotFoundHttpException("Invalid userid in request.");
        }

        if (null === $thread || null === $comment || $comment->getThread() !== $thread) {
            throw new NotFoundHttpException(sprintf("No comment with id '%s' found for thread with id '%s'", $commentId, $id));
        }

        if(in_array($request->get('state'),Comment::getModerationStates())){
            $comment->setModerationState($request->get('state'));
            $view = View::create()
                ->setData(array('comment' => $comment, 'thread' => $thread))
                ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment'));

            $this->addModerationCache($view);

            return $this->getViewHandler()->handle($view);
        }
        throw new \Exception("Invalid moderation state : ".$request->get('state'), 404);
    }

    /**
     * Edits a given comment.
     *
     * @param Request $request Current request
     * @param string $id Id of the thread
     * @param mixed $commentId Id of the comment
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return View
     */
    public function putThreadCommentsAction(Request $request, $id, $commentId)
    {
        $commentManager = $this->container->get('la_comment.manager.comment');

        $thread = $this->container->get('la_comment.manager.thread')->findThreadById($id);
        $comment = $commentManager->findCommentById($commentId);

        if (is_null($request->get('userid'))) {
            throw new NotFoundHttpException("No userid found in request.");
        }
        if ($comment->getUserId() != $request->get('userid')) {
            throw new NotFoundHttpException("Invalid userid in request.");
        }

        if (null === $thread || null === $comment || $comment->getThread() !== $thread) {
            throw new NotFoundHttpException(sprintf("No comment with id '%s' found for thread with id '%s'", $commentId, $id));
        }

        $form = $this->container->get('la_comment.form_factory.comment')->createForm();

        $form->setData($comment);
        $form->submit($request);

        if ($form->isValid()) {

            if ($commentManager->saveComment($comment) !== false) {
                return $this->getViewHandler()->handle($this->onFormSuccess($form));
            }
        }

        return $this->getViewHandler()->handle($this->onEditCommentError($form, $id, $comment->getParent()));
    }

    /**
     * Get the comments of a thread. Creates a new thread if none exists.
     *
     * @param Request $request Current request
     * @param string $id Id of the thread
     * @throws \Exception
     * @internal param null $offset
     * @internal param null $limit
     * @return View
     * @todo Add support page/pagesize/sorting/tree-depth parameters
     */
    public function getThreadCommentsAction(Request $request, $id)
    {
        if (is_null($id) || '' === $id) {
            throw new \Exception(sprintf('ThreadId is empty'));
        }
        $displayDepth = $request->query->get('displayDepth');
        $sorter = $request->query->get('sorter');
        $offset = $request->query->get('offset');
        $limit = $request->query->get('limit');
        $state = $request->query->get('state');

        if (is_null($sorter)) {
            $sorter = $this->container->getParameter('la_comment.sorting_factory.default_sorter');
        }

        $threadManager = $this->container->get('la_comment.manager.thread');
        $commentManager = $this->container->get('la_comment.manager.comment');


        // cache
        $thread = $this->getThreadFromCache($id);
        // end

        // We're now sure it is no duplicate id, so create the thread
        if (null === $thread) {

            // Decode the permalink for cleaner storage (it is encoded on the client side)
            $permalink = urldecode($request->query->get('permalink'));
            if (is_null($permalink) || empty($permalink)) {
                $permalink = $request->getUri();
            }

            $thread = $threadManager->createThread();
            $thread->setId($id);
            $thread->setPermalink($permalink);

            // Validate the entity
            $validator = $this->get('validator');
            $errors = $validator->validate($thread, 'NewThread');
            if (count($errors) > 0) {
                $view = View::create()
                    ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                    ->setData(array('errors' => $errors))
                    ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'errors'));

                return $this->getViewHandler()->handle($view);
            }

            // Add the thread
            $threadManager->saveThread($thread);

            // cache new thread
            $thread = $this->getThreadFromCache($id);
            if (is_null($thread)) {
                throw new \Exception(sprintf('getThreadFromCache returns null thread (id=%s)', $id));
            }
        }

        $viewMode = $request->query->get('view', 'tree');
        $cacheManager = $this->getCacheManager();

        switch ($viewMode) {
            case self::VIEW_FLAT:
                // cache
                $comments = $cacheManager->get(
                    array_merge($cacheManager->getArrayKeysForThreadCache($thread->getId()), array($displayDepth, $sorter, $offset, $limit, $state)),
                    $cacheManager->getArrayKeysForThreadCache($id), // tag = thread_id
                    function () use ($commentManager, $thread, $displayDepth, $sorter, $offset, $limit, $state) {
                        return $commentManager->findCommentsByThread($thread, $displayDepth, $sorter, $offset, $limit, $state);
                    },
                    $this->container->getParameter('la_comment.cache.thread_cache_ttl')
                );

                // end

                // We need nodes for the api to return a consistent response, not an array of comments
                $comments = array_map(function ($comment) {
                        return array('comment' => $comment, 'children' => array());
                    },
                    $comments
                );
                break;

            case self::VIEW_TREE:
            default:
                // cache
                $comments = $cacheManager->get(
                    array_merge($cacheManager->getArrayKeysForThreadCache($thread->getId()), array($displayDepth, $sorter, $offset, $limit, $state)),
                    $cacheManager->getArrayKeysForThreadCache($id), // tag = thread_id
                    function () use ($commentManager, $thread, $displayDepth, $sorter, $offset, $limit, $state) {
                        return $commentManager->findCommentTreeByThread($thread, $sorter, $displayDepth, $offset, $limit, $state);
                    },
                    $this->container->getParameter('la_comment.cache.thread_cache_ttl')
                );
                // end

                break;
        }

        $view = View::create()
            ->setData(array(
                'comments' => $comments,
                'displayDepth' => $displayDepth,
                'sorter' => $sorter,
                'thread' => $thread,
                'view' => $viewMode,
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comments'));

        // Register a special handler for RSS. Only available on this route.
        if ('rss' === $request->getRequestFormat()) {
            $templatingHandler = function ($handler, $view, $request) {
                $view->setTemplate(new TemplateReference('LACommentBundle', 'Thread', 'thread_xml_feed'));

                $this->addModerationCache($view);

                return new Response($handler->renderTemplate($view, 'rss'), Codes::HTTP_OK, $view->getHeaders());
            };

            $this->get('fos_rest.view_handler')->registerHandler('rss', $templatingHandler);
        }

        $this->addModerationCache($view);

        return $this->getViewHandler()->handle($view);
    }

    /**
     * Creates a new Comment for the Thread from the submitted data.
     *
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \La\CommentBundle\Exception\UserNotAllowedToPostException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function postThreadCommentsAction(Request $request, $id)
    {
        try {
            /* @todo Remplacer erreurs par translations */

            $parameters = $request->get('la_comment_comment');

            if (isset($parameters['userId'])) {
                $userId = $parameters['userId'];
            } else {
                throw new NotFoundHttpException(sprintf('No userId was found'));
            }
            if (isset($parameters['cookie'])) {
                $cookie = $parameters['cookie'];
            } else {
                throw new NotFoundHttpException(sprintf('No cookie was found'));
            }

            /** @var Thread $thread */
            $thread = $this->container->get('la_comment.manager.thread')->findThreadById($id);
            if (!$thread) {
                throw new NotFoundHttpException(sprintf('Thread with identifier of "%s" does not exist', $id));
            }

            $commentManager = $this->container->get('la_comment.manager.comment');

            if (!$commentManager->isUserAllowedToPost($thread, $userId)) {
                throw new ApiException\UserNotAllowedToPostException("User is not allowed to post.");
            }
            $parent = $this->getValidCommentParent($thread, $request->query->get('parentId'));

            if (!is_null($parent) && !$commentManager->isUserAllowedToAnswer($thread, $userId)) {
                throw new ApiException\UserNotAllowedToPostException("User is not allowed to answer.");
            }

            /** @var Comment $comment */
            $comment = $commentManager->createComment($thread, $parent);
            $comment->setCookie($cookie);

            $form = $this->container->get('la_comment.form_factory.comment')->createForm();
            $form->setData($comment);
            $form->submit($request);

            if ($form->isValid()) {

                if ($commentManager->saveComment($comment) !== false) {
                    return $this->getViewHandler()->handle($this->onFormSuccess($form, Codes::HTTP_CREATED));
//                    return $this->getViewHandler()->handle($this->onFormSuccess($form, $id, $parent));
                }
            }

            return $this->getViewHandler()->handle($this->onCreateCommentError($form, $id, $parent));
        } catch (\Exception $e) {
            return View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData($e->getMessage());
        }
    }

    /**
     * Creates a new Vote for the Comment from the submitted data.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $id Id of the thread
     * @param mixed $commentId Id of the comment
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return View
     */
    public function postThreadCommentVotesAction(Request $request, $id, $commentId)
    {

        $thread = $this->getThreadFromCache($id);

        $comment = $this->container->get('la_comment.manager.comment')->findCommentById($commentId);

        if (null === $thread || null === $comment || $comment->getThread() !== $thread) {
            throw new NotFoundHttpException(sprintf("No comment with id '%s' found for thread with id '%s'", $commentId, $id));
        }

        $voteManager = $this->container->get('la_comment.manager.vote');
        $vote = $voteManager->createVote($comment);

        $form = $this->container->get('la_comment.form_factory.vote')->createForm();
        $form->setData($vote);
        $form->submit($request);

        if ($form->isValid()) {
            $vote = $form->getData();

            if ($voteManager->saveVote($vote) !== false) {

                $view = View::create()
                    ->setData($comment)
                    ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment'));

                return $this->getViewHandler()->handle($view);
            }

            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array(
                    'id' => $id,
                    'comment' => $comment
                ))
                ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment_new'));

            return $view;
        }
        return $this->getViewHandler()->handle($this->onCreateVoteError($form, $id, $commentId));
    }

    /**
     * Get user information for a thread. Useful for frontend integration.
     *
     * @param Request $request
     * @param $id
     * @param $userId
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getThreadUserAction(Request $request, $id, $userId)
    {
        die('v2');
        $threadManager = $this->container->get('la_comment.manager.thread');

        $thread = $this->getThreadFromCache($id);

        if (null === $thread || null === $userId) {
            throw new NotFoundHttpException(sprintf("No user or valid thread id."));
        }

        $commentManager = $this->container->get('la_comment.manager.comment');

        $threadUserInfo = array(
            $thread->getId() => array(
                $userId => array(
                    "has_posted" => $commentManager->hasUserPosted($thread, $userId), // A posté dans le thread
                    "has_rated" => $commentManager->hasUserRated($thread, $userId), // A noté le thread
                    "has_voted" => $commentManager->hasUserVoted($thread, $userId, $this->container->get('la_comment.manager.vote')), // A voté dans le thread ( array(commentid => vote, comment_id => vote ...) ou false
                    "can_post" => $commentManager->isUserAllowedToPost($thread, $userId), // Peut poster
                    "can_rate" => $commentManager->isUserAllowedToRate($thread, $userId), // Peut noter
                    "is_banned" => $commentManager->isUserBanned($thread, $userId), // Est banni (@todo)
                    "has_subscribed" => $threadManager->hasUserSubscribed($thread, $userId) // S'est inscrit aux notifications par email (@todo)
                )
            )
        );

        // If request is null, method was called from within the controller : no json encode.
        return is_null($request) ? $threadUserInfo : json_encode($threadUserInfo, JSON_PRETTY_PRINT);
    }

//////////////////////////////////////////////////// BELOW : <NOT> CONTROLLER ACTIONS //////////////////////////////////////////////////////////


    /**
     * On success
     *
     * @param FormInterface $form
     * @param $statusCode
     * @return View
     */
    protected function onFormSuccess(FormInterface $form, $statusCode = Codes::HTTP_OK)
    {
        $data = $form->getData();

        // cache
        $cacheManager = $this->getCacheManager();
        if ($data instanceof Thread) {
            $arrayKeys = $cacheManager->getArrayKeysForThreadCache($data->getId());
            $arrayTags = $cacheManager->getArrayKeysForThreadCache($data->getId());
            $ttl = $this->container->getParameter('la_comment.cache.thread_cache_ttl');

            $cacheManager->get(
                $arrayKeys,
                $arrayTags,
                function () use ($data) {
                    return $data;
                },
                $ttl
            );
        }
        return $this->view($data, $statusCode);
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form
     *
     * @return View
     */
    protected function onCreateThreadError(FormInterface $form)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'new'));

        return $view;
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the Thread creation fails due to a duplicate id.
     *
     * @param FormInterface $form
     *
     * @return View
     */
    protected function onCreateThreadErrorDuplicate(FormInterface $form)
    {
        return new Response(sprintf("Duplicate thread id '%s'.", $form->getData()->getId()), Codes::HTTP_BAD_REQUEST);
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form Form with the error
     * @param string $id Id of the thread
     * @param CommentInterface $parent Optional comment parent
     *
     * @return View
     */
    protected function onCreateCommentError(FormInterface $form, $id, CommentInterface $parent = null)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
                'id' => $id,
                'parent' => $parent,
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment_new'));

        return $view;
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form Form with the error
     * @param string $id Id of the thread
     * @param mixed $commentId Id of the comment
     *
     * @return View
     */
    protected function onCreateVoteError(FormInterface $form, $id, $commentId)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'id' => $id,
                'commentId' => $commentId,
                'form' => $form,
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'vote_new'));

        return $view;
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form Form with the error
     * @param string $id Id of the thread
     *
     * @return View
     */
    protected function onEditCommentError(FormInterface $form, $id)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
                'comment' => $form->getData(),
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment_edit'));

        return $view;
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form
     *
     * @return View
     */
    protected function onOpenThreadError(FormInterface $form)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
                'id' => $form->getData()->getId(),
                'isCommentable' => $form->getData()->isCommentable(),
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'commentable'));

        return $view;
    }

    /**
     * Forwards the action to the comment view on a successful form submission.
     *
     * @param FormInterface $form Comment delete form
     * @param integer $id Thread id
     *
     * @return View
     */
    protected function onRemoveThreadCommentSuccess(FormInterface $form, $id)
    {
        return RouteRedirectView::create('la_comment_get_thread_comment', array('id' => $id, 'commentId' => $form->getData()->getId()));
    }

    /**
     * Returns a HTTP_BAD_REQUEST response when the form submission fails.
     *
     * @param FormInterface $form Comment delete form
     * @param integer $id Thread id
     *
     * @return View
     */
    protected function onRemoveThreadCommentError(FormInterface $form, $id)
    {
        $view = View::create()
            ->setStatusCode(Codes::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
                'id' => $id,
                'value' => $form->getData()->getState(),
            ))
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment_remove'));

        return $view;

    }

    /**
     * Checks if a comment belongs to a thread. Returns the comment if it does.
     *
     * @param ThreadInterface $thread Thread object
     * @param mixed $commentId Id of the comment.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return CommentInterface|null The comment.
     */
    private function getValidCommentParent(ThreadInterface $thread, $commentId)
    {
        if (null !== $commentId) {
            $comment = $this->container->get('la_comment.manager.comment')->findCommentById($commentId);
            if (!$comment) {
                throw new NotFoundHttpException(sprintf('Parent comment with identifier "%s" does not exist', $commentId));
            }

            if ($comment->getThread() !== $thread) {
                throw new NotFoundHttpException('Parent comment is not a comment of the given thread.');
            }

            return $comment;
        }
    }

    /**
     * @return \FOS\RestBundle\View\ViewHandler
     */
    private function getViewHandler()
    {
        return $this->container->get('fos_rest.view_handler');
    }

    protected function getErrors($form, $request, $groups = null)
    {
        $form->submit($request);

        if (!$form->isValid()) {

            $errors = array();
            $errorList = $this->get('validator')->validate($form->getData(), $groups);

            foreach ($errorList as $error) {
                $propertyPath = $error->getPropertyPath();
                $message = $this->container->get('translator')->trans($error->getMessageTemplate(), $error->getMessageParameters(), 'formapi');

                $explodedPath = explode('.', $propertyPath);
                if ($explodedPath[count($explodedPath) - 1] == 'data') {
                    array_pop($explodedPath);
                }

                $final = $this->getChildren($explodedPath, $message);
                $errors = array_merge($errors, $final);

                $tmp = array();
                $final = array();
                foreach ($explodedPath as $key) {

                    if (empty($final)) {
                        $final = $message;
                    } else {
                        $final[$key] = $tmp;
                    }
                }
            }
            return $errors;
        } else {
            return false;
        }
    }

    protected function addModerationCache($view)
    {
//        if ($this->container->getParameter('la_comment.moderation.a_priori')) {
//            $ttl = $this->container->getParameter('la_comment.moderation.cache_default_ttl');
//            $view->setHeader('cache-control', sprintf('max-age=%d', $ttl));
//            $view->getResponse()->headers->addCacheControlDirective('public');
//            $view->getResponse()->headers->removeCacheControlDirective('private');
//        }

    }

    protected function getThreadFromCache($id)
    {
        $manager = $this->container->get('la_comment.manager.thread');
        // cache
        $cacheManager = $this->getCacheManager();
        $thread = $this->getCacheManager()->get(
            $cacheManager->getArrayKeysForThreadCache($id),
            $cacheManager->getArrayKeysForThreadCache($id),
            function () use ($manager, $id) {
                return $manager->findThreadById($id);
            },
            $this->container->getParameter('la_comment.cache.thread_cache_ttl')
        );
        // end

        // Le thread null a été mis en cache
        if (is_null($thread)) {
            // on le décache
            $cacheManager->deleteByTags($cacheManager->getArrayKeysForThreadCache($id));
        }
        return $thread;
    }

    protected function getCacheManager()
    {
        return $this->get('la_comment.cache.manager');
    }
}
