<?php
namespace La\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use La\CommentBundle\Model\CommentInterface;

class ModerationController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function homeAction(Request $request)
    {
        return $this->get('templating')->renderResponse('LaCommentBundle:Moderation:home.html.twig', array());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function pendingAction(Request $request)
    {
        $manager = $this->container->get('la_comment.manager.moderation');
        $comments = $manager->findLastPendingComments();
        $causes = $manager->getModerationCauses();

        return $this->get('templating')->renderResponse('LaCommentBundle:Moderation:pending.html.twig',
            array(
                "comments" => $comments,
                "causes" => $causes
            ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function recentsAction(Request $request)
    {
        $manager = $this->container->get('la_comment.manager.moderation');
        $comments = $manager->findRecentlyModeratedComments();
        $causes = $manager->getModerationCauses();

        return $this->get('templating')->renderResponse('LaCommentBundle:Moderation:recents.html.twig',
            array(
                "comments" => $comments,
                "causes" => $causes
            ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return bool
     */
    public function validateAction(Request $request, $id)
    {
        try {
            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }
            $moderation = $this->container->get('la_comment.manager.moderation');
            $manager = $this->container->get('la_comment.manager.comment');

            $comment = $manager->findCommentById($id);
            $moderation->setModerated($comment, $moderation::MODERATION_OK, null, $request->getUser(), new \DateTime());

            $manager->saveComment($comment);

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param $cause
     * @return bool
     */
    public function rejectAction(Request $request, $id, $cause)
    {
        try {

            if (!$request->isXmlHttpRequest()) {
                return new Response('XmlHttpRequests only.', 400);
            }

            $moderation = $this->container->get('la_comment.manager.moderation');
            $manager = $this->container->get('la_comment.manager.comment');

            $comment = $manager->findCommentById($id);
            $moderation->setModerated($comment, $moderation::MODERATION_NOT_OK, urldecode($cause), $request->getUser(), new \DateTime());

            $manager->saveComment($comment);

            return new Response(null, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param $until
     * @param $message
     */
    public function banAction(Request $request, $id, $until, $message)
    {
        /** @TODO */
        die('coming soon');
        $this->container->get("la_comment.manager.moderation")->ban($id, $until, $message, $request->getUser());
    }


}