<?php
namespace La\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\Request;
use La\CommentBundle\Entity\Alert;
use La\CommentBundle\Events;

/**
 * Class AlertController
 * @package La\CommentBundle\Controller
 */
class AlertController extends Controller
{

    /**
     * @param Request $request
     * @param $commentId
     * @return mixed
     */
    public function alertAction(Request $request, $commentId)
    {
        $dispatcher = $this->container->get("event_dispatcher");
        $templating = $this->container->get('templating');
        $commentManager = $this->container->get('la_comment.manager.comment.default');
        $alertManager = $this->container->get('la_comment.manager.alert.default');

        $comment = $commentManager->findCommentById($commentId);
        if (is_null($comment)) {
            return $templating->renderResponse('LaCommentBundle:Alert:error.html.twig');
        }

        $alert = new Alert($comment);
        $form = $this->container->get('form.factory')->create('la_comment_alert', $alert, array('commentManager' => $commentManager));

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if ($form->isValid()) {
                $alert->setHeaders($alertManager->getSerializedHeaders($request));
                $result = $alertManager->createAndSaveAlert($alert);
                $dispatcher->dispatch(Events::ALERT_REQUEST, new GenericEvent($alert));
                return $templating->renderResponse('LaCommentBundle:Alert:success.html.twig');
            }
            return $templating->renderResponse('LaCommentBundle:Alert:alert.html.twig', array('form' => $form->createView()));
        }

        $dispatcher->dispatch(Events::ALERT_CREATE_FORM, new FormEvent($form, $form->getData()));

        return $templating->renderResponse('LaCommentBundle:Alert:alert.html.twig', array('form' => $form->createView()));
    }
}

?>