<?php
namespace La\CommentBundle\Exception;

use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception\ApiExceptionInterface;

/**
 * Class CreateThreadDuplicateException
 * @package La\CommentBundle\Exception
 */
class CreateThreadDuplicateException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_UNAUTHORIZED;
    }

    public function getApiMessage()
    {
        return 'create_thread_duplicate';
    }

}