<?php
namespace La\CommentBundle\Exception;

use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception\ApiExceptionInterface;

/**
 * Class UserNotAllowedToPostException
 * @package La\CommentBundle\Exception
 */
class UserNotAllowedToPostException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_UNAUTHORIZED;
    }

    public function getApiMessage()
    {
        return 'user_not_allowed_to_post';
    }

}