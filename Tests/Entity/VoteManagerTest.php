<?php



namespace La\CommentBundle\Tests\Entity;

use La\CommentBundle\Entity\VoteManager;

/**
 * Tests the functionality provided by Entity\VoteManager.
 *
 */
class VoteManagerTest extends \PHPUnit_Framework_TestCase
{
    protected $dispatcher;
    protected $em;
    protected $repository;
    protected $class;
    protected $classMetadata;

    public function setUp()
    {
        if (!class_exists('Doctrine\\ORM\\EntityManager')) {
            $this->markTestSkipped('Doctrine ORM not installed');
        }

        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();
        $this->repository = $this->getMockBuilder('Doctrine\ORM\EntityRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $this->class = 'La\CommentBundle\Tests\Entity\Vote';

        $this->em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($this->repository));

        $this->classMetadata = new \stdClass();
        $this->classMetadata->name = $this->class;

        $this->em->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->class)
            ->will($this->returnValue($this->classMetadata));
    }

    public function testGetClass()
    {


        $manager = new VoteManager($this->dispatcher, $this->em, $this->class);

        $this->assertEquals($this->class, $manager->getClass());
    }

    public function testAddVote()
    {


        $vote = $this->getMock('La\CommentBundle\Model\VoteInterface');
        $vote->expects($this->any())
            ->method('getComment')
            ->will($this->returnValue($this->getMock('La\CommentBundle\Model\CommentInterface')));

        $this->em->expects($this->exactly(2))
            ->method('persist');

        $this->em->expects($this->once())
            ->method('flush');

        $manager = new VoteManager($this->dispatcher, $this->em, $this->class);
        $manager->saveVote($vote);
    }

    public function testFindVoteBy()
    {


        $vote = $this->getMock('La\CommentBundle\Model\VoteInterface');
        $criteria = array('id' => 123);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with($criteria)
            ->will($this->returnValue($vote));

        $manager = new VoteManager($this->dispatcher, $this->em, $this->class);
        $result = $manager->findVoteBy($criteria);

        $this->assertEquals($vote, $result);
    }

    public function testFindVoteById()
    {


        $id = 123;
        $vote = $this->getMock('La\CommentBundle\Model\VoteInterface');

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(array('id' => $id))
            ->will($this->returnValue($vote));

        $manager = new VoteManager($this->dispatcher, $this->em, $this->class);
        $result = $manager->findVoteById($id);

        $this->assertEquals($vote, $result);
    }

    public function testCreateVote()
    {


        $comment = $this->getMock('La\CommentBundle\Model\CommentInterface');

        $manager = new VoteManager($this->dispatcher, $this->em, $this->class);
        $result = $manager->createVote($comment);

        $this->assertInstanceOf('La\CommentBundle\Model\VoteInterface', $result);
    }
}
