<?php



namespace La\CommentBundle\Tests\Entity;

class CommentTest extends \PHPUnit_Framework_TestCase
{
    public function testSetAncestorsAddsDepth()
    {


        $ancestors = array(1, 5, 12, 14);
        $comment = new Comment();
        $comment->setAncestors($ancestors);

        $this->assertEquals(count($ancestors), $comment->getDepth());
    }

    public function testSetParentSetsAncestors()
    {


        $ancestors = array(1, 5, 12);
        $parentId = 14;

        $parent = $this->getMock('La\CommentBundle\Entity\Comment');
        $parent->expects($this->once())
            ->method('getAncestors')
            ->will($this->returnValue($ancestors));
        $parent->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($parentId));

        $comment = new Comment();
        $comment->setParent($parent);

        $this->assertEquals(array_merge($ancestors, array($parentId)), $comment->getAncestors());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testSetParentNotPersisted()
    {


        $parent = $this->getMock('La\CommentBundle\Entity\Comment');
        $parent->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(null));

        $comment = new Comment();
        $comment->setParent($parent);
    }
}
