<?php



namespace La\CommentBundle\Tests\Entity;

use La\CommentBundle\Entity\Comment as BaseComment,
    La\CommentBundle\Model\ThreadInterface;

class Comment extends BaseComment
{
    /**
     * Thread of this comment
     *
     * @var ThreadInterface
     */
    protected $thread;

    /**
     * @return ThreadInterface
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param  ThreadInterface $thread
     * @return null
     */
    public function setThread(ThreadInterface $thread)
    {
        $this->thread = $thread;
    }
}
