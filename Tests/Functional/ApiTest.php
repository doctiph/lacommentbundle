<?php

namespace La\CommentBundle\Tests\Functional;

use La\CommentBundle\Entity\Thread;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

/**
 * Functional tests of the CommentBundle api.
 *
 * @group functional
 */
class ApiTest extends WebTestCase
{
    /** @var  Client */
    protected $client;

    protected function setUp()
    {
        $this->client = self::createClient(array(), array(
            'PHP_AUTH_USER' => 'test',
            'PHP_AUTH_PW' => 'test',
        ));
    }

    /**
     * Tests retrieval of a thread that doesnt exist.
     *
     * la_comment_get_thread: GET: /api/threads/{id}.{_format}
     */
    public function testGetThread404()
    {
        $this->client->insulate(true);


        $this->client->request('GET', '/api/threads/this-will-probably-end-in-a-404');

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Tests creation of a thread that doesnt exist.
     *
     * la_comment_post_threads: POST: /api/threads.{_format}
     *
     * @return string The id of the created thread
     */
    public function testPostNewThread()
    {
        try {


            $id = uniqid();
            $type = Thread::THREAD_TYPE;

            $this->client->request('POST', '/api/threads', array(
                'la_comment_thread' => array(
                    'id' => $id,
                    'type' => $type,
                    'permalink' => "http://unit.tests.comments/api/threads/$id"
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());


            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($id, $content->id);
            $this->assertEquals($type, $content->type);

            return $id;
        } catch (\Exception $e) {

            $this->markTestIncomplete($e->getMessage());
        }

    }

    /**
     * Tests retrieval of an existing thread.
     *
     * la_comment_get_thread: GET: /api/threads/{id}.{_format}
     *
     * @param mixed $id
     * @depends testPostNewThread
     */
    public function testGetThread($id)
    {
        try {


            $this->client->request('GET', "/api/threads/$id");

            $content = json_decode($this->client->getResponse()->getContent());

            $this->assertFalse(is_null($content));
            $this->assertEquals($id, $content->thread->id);
        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Tests retrieval of an empty thread.
     *
     * la_comment_post_thread_comments: POST: /api/threads/{id}/comments.{_format}
     *
     * @param mixed $id
     * @return mixed
     * @depends testPostNewThread
     */
    public function testGetEmptyThread($id)
    {
        try {


            $this->client->request('GET', "/api/threads/$id/comments");

            $content = json_decode($this->client->getResponse()->getContent());

            $this->assertFalse(is_null($content));
            $this->assertEquals($id, $content->thread->id);
            $this->assertEquals(0, $content->thread->num_comments);
            $this->assertEmpty($content->comments);

            return $id;
        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }

    }

    /**
     * Tests addition of a comment to a thread.
     *
     * la_comment_post_thread_comments: POST: /api/threads/{id}/comments.{_format}
     * la_comment_get_thread_comment: GET: /api/threads/{id}/comments/{commentId}.{_format}
     *
     * @param mixed $id
     * @return mixed
     * @depends testGetEmptyThread
     */
    public function testAddValidCommentToThread($id)
    {
        try {


            $body = "Quel talent !";
            $headers = serialize(array("HEADER" => "HEADERDETEST", "Test2" => "header de test"));
            $cookie = "la_cfb_user=ee9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 1;

            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());

            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($body, $content->body);
            $this->assertEquals($headers, $content->headers);
            $this->assertEquals($cookie, $content->cookie);
            $this->assertEquals($userId, $content->user_id);


            return array('thread' => $id, 'comment' => $content->id);

        } catch (\Exception $e) {

            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Tests addition of an invalid comment to a thread.
     *
     * @param mixed $id
     * @return mixed
     * @depends testGetEmptyThread
     */
    public function testAddInvalidCommentToThread($id)
    {
        try {


            /** VALID TEST PARAMS */
            $body = "Quel talent !";
            $headers = serialize(array("HEADER" => "HEADERDETEST", "Test2" => "header de test"));
            $cookie = "la_cfb_user=ee9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 1;

            /*** TEST 1 : Body-less comment */
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => '',
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertNotEmpty($content->errors->children->body->errors);

            /*** TEST 2 : Headers-less comment */
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertNotEmpty($content->errors->children->headers->errors);

            /*** TEST 3 : Cookie-less comment */
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals("No cookie was found", $content);

            /*** TEST 4 : User-less comment */
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals("No userId was found", $content);

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Replies to an existing comment.
     *
     * la_comment_get_thread_comments: GET: /api/threads/{id}/comments.{_format}
     * la_comment_new_thread_comments: GET: /api/threads/{id}/comments/new.{_format}
     * la_comment_get_thread_comment: GET: /api/threads/{id}/comments/{commentId}.{_format}
     *
     * @param array $ids
     * @return mixed
     * @depends testAddValidCommentToThread
     */
    public function testReplyToComment(array $ids)
    {
        // sleep a second to create different 'createdAt' dates
        sleep(1);

        try {


            $thread = $ids['thread'];
            $parent = $ids['comment'];

            $body = "C'est bien vrai !";
            $headers = serialize(array("HEADER" => "HEADERDETESTDEUX", "Test2" => "header de test deux"));
            $cookie = "la_cfb_user=fe9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 2;

            $this->client->request('POST', "/api/threads/$thread/comments?parentId=$parent", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());

            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($parent, $content->ancestors);
            $this->assertEquals(1, $content->depth);
            $this->assertEquals($body, $content->body);
            $this->assertEquals($headers, $content->headers);
            $this->assertEquals($cookie, $content->cookie);
            $this->assertEquals($userId, $content->user_id);

            return array(
                'thread' => $thread,
                'comments' => array(
                    $parent,
                    $content->id
                )
            );
        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Tests that there are 2 comments in a tree.
     *
     * la_comment_get_thread_comments: GET: /api/threads/id/comments
     *
     * @param array $ids
     * @depends testReplyToComment
     */
    public function testGetCommentTree(array $ids)
    {

        try {
            $thread = $ids['thread'];
            $this->client->request('GET', "/api/threads/$thread/comments");

            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals(2, $content->thread->num_comments);
        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Tests that there is only 1 comment in the tree.
     *
     * la_comment_get_thread_comments: GET: /api/threads/{id}/comments.{_format}?displayDepth=0
     *
     * @param array $ids
     * @depends testReplyToComment
     */
    public function testGetCommentTreeDepth(array $ids)
    {


        $thread = $ids['thread'];

        $this->client->request('GET', "/api/threads/$thread/comments?displayDepth=0");

        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(1, count($content->comments));
    }

    /**
     * Tests that there are 2 comments in a thread. Rendered both on level 0.
     *
     * la_comment_get_thread_comments: GET: /api/threads/{id}/comments.{_format}?view=flat
     *
     * @param array $ids
     * @depends testReplyToComment
     */
    public function testGetCommentFlat(array $ids)
    {


        $thread = $ids['thread'];

        $this->client->request('GET', "/api/threads/$thread/comments?view=flat");

        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(2, count($content->comments));
    }

    /**
     * Tests that there are 2 comments in a thread. Rendered both on level 0. Sorted by date asc/desc.
     *
     * la_comment_get_thread_comments: GET: /api/threads/{id}/comments.{_format}?view=flat&sorter=date_asc/date_desc
     *
     * @param array $ids
     * @return array
     * @depends testReplyToComment
     */
    public function testGetCommentFlatSorted(array $ids)
    {


        $thread = $ids['thread'];

        $this->client->request('GET', "/api/threads/$thread/comments?view=flat&sorter=date_desc");
        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(2, count($content->comments));

        $this->client->request('GET', "/api/threads/$thread/comments?view=flat&sorter=date_asc");
        $content2 = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content2));
        $this->assertEquals(2, count($content->comments));

        $this->assertEquals($content->comments[0]->comment->id, $content2->comments[1]->comment->id);
        $this->assertEquals($content->comments[1]->comment->id, $content2->comments[0]->comment->id);

        return array(
            'thread' => $thread,
            'comments' => array(
                $content->comments[0]->comment->id,
                $content->comments[1]->comment->id
            )
        );
    }

    /**
     * Test vote of a comment.
     *
     * @param array $ids
     * @return mixed
     * @depends testReplyToComment
     */
    public function testVoteComment(array $ids)
    {


        $thread = $ids['thread'];
        $comment = $ids['comments'][0];
        $comment2 = $ids['comments'][1];

        $this->client->request('POST', "/api/threads/$thread/comments/$comment/votes", array(
            'la_comment_vote' => array(
                'value' => 1,
                'user' => 1,
            ),
        ));
        $content = json_decode($this->client->getResponse()->getContent());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(1, $content->upvotes, "The 'upvotes' field does not match :");
        $this->assertEquals(0, $content->downvotes, "The 'downvotes' field does not match :");
        $this->assertEquals(1, $content->score, "The 'score' field does not match :");

        $this->client->request('POST', "/api/threads/$thread/comments/$comment2/votes", array(
            'la_comment_vote' => array(
                'value' => -1,
                'user' => 1,
            ),
        ));
        $content = json_decode($this->client->getResponse()->getContent());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(0, $content->upvotes, "The 'upvotes' field does not match :");
        $this->assertEquals(1, $content->downvotes, "The 'downvotes' field does not match :");
        $this->assertEquals(-1, $content->score, "The 'score' field does not match :");

        return $thread;
    }

    /**
     * Test sort comments by score (votes).
     *
     * @param $id
     * @return mixed
     * @depends testVoteComment
     */
    public function testGetCommentFlatSortedByScore($id)
    {


        $this->client->request('GET', "/api/threads/$id/comments?view=flat&sorter=score_desc");
        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(2, count($content->comments));

        $this->client->request('GET', "/api/threads/$id/comments?view=flat&sorter=score_asc");
        $content2 = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content2));
        $this->assertEquals(2, count($content->comments));

        $this->assertEquals($content->comments[0]->comment->id, $content2->comments[1]->comment->id);
        $this->assertEquals($content->comments[1]->comment->id, $content2->comments[0]->comment->id);

        return array(
            'thread' => $id,
            'comments' => array(
                $content->comments[0]->comment->id,
                $content->comments[1]->comment->id
            )
        );
    }

    /**
     * Cleanup of created entities
     *
     * @param array $ids
     * @depends testGetCommentFlatSorted
     */
    public function testCleanup(array $ids)
    {
        try {


            /** @var \Doctrine\ORM\EntityManager $em */
            $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');

            foreach ($ids['comments'] as $comment) {
                $em->remove($em->find("La\\CommentBundle\\Entity\\Comment", $comment));
            }
            $em->remove($em->find("La\\CommentBundle\\Entity\\Thread", $ids['thread']));

            $em->flush();

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }


    /********************** TEST REVIEW-SPECIFIC BEHAVIOURS **********************/


    /**
     * Tests creation of a review that doesnt exist.
     *
     * @return string The id of the created thread
     */
    public function testPostNewReview()
    {
        try {


            $id = uniqid();
            $type = Thread::REVIEW_TYPE;

            $this->client->request('POST', '/api/threads', array(
                'la_comment_thread' => array(
                    'id' => $id,
                    'type' => $type,
                    'permalink' => "http://unit.tests.comments/api/threads/$id"
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());


            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($id, $content->id);
            $this->assertEquals($type, $content->type);

            return $id;
        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }

    }

    /**
     * Tests addition of a comment to a review.
     *
     * @param mixed $id
     * @return mixed
     * @depends testPostNewReview
     */
    public function testAddValidCommentToReview($id)
    {
        try {


            $body = "Quel talent !";
            $rating = 3;
            $headers = serialize(array("HEADER" => "HEADERDETEST", "Test2" => "header de test"));
            $cookie = "la_cfb_user=ee9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 1;

            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'rating' => $rating,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($body, $content->body);
            $this->assertEquals($headers, $content->headers);
            $this->assertEquals($rating, $content->rating);
            $this->assertEquals($cookie, $content->cookie);
            $this->assertEquals($userId, $content->user_id);

            $this->assertEquals($rating, $content->thread->rating);
            $this->assertEquals($rating, $content->thread->rating_sum);
            $this->assertEquals(1, $content->thread->rating_count);

            return array('thread' => $id, 'comment' => $content->id);

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

    /**
     * Tests addition of an invalid comment to a review.
     *
     * @param array $ids
     * @return mixed
     * @depends testAddValidCommentToReview
     */
    public function testAddInvalidCommentToReview(array $ids)
    {
        try {


            $id = $ids['thread'];
            $firstCommentId = $ids['comment'];

            /** VALID TEST PARAMS */
            $body = "Quel talent !";
            $headers = serialize(array("HEADER" => "HEADERDETEST", "Test2" => "header de test"));
            $cookie = "la_cfb_user=ee9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 1;

            /*** TEST 1 : User already posted a review ***/

            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId,
                    'rating' => 4
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));

            $userId = 2;


            /*** TEST 2 : Reply on a review IF replies are not allowed on a review ***/

            if (static::$kernel->getContainer()->getParameter('la_comment.config.allow_replies_on_reviews') === false) {

                try {

                    $this->client->request('POST', "/api/threads/$id/comments?parentId=$firstCommentId", array(
                        'la_comment_comment' => array(
                            'body' => $body,
                            'headers' => $headers,
                            'cookie' => $cookie,
                            'userId' => $userId,
                            'rating' => 4
                        ),
                    ));

                    $content = json_decode($this->client->getResponse()->getContent());
                    $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
                    $this->assertFalse(is_null($content));

                } catch (\Exception $e) {
                    $this->markTestIncomplete($e->getMessage());
                }
            }

            /*** TEST 3 : Rating-less comment ***/
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId,
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertNotEmpty($content->errors->children->rating->errors);

            /*** TEST 4 : Foolish rating comment ***/
            $this->client->request('POST', "/api/threads/$id/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'cookie' => $cookie,
                    'userId' => $userId,
                    'rating' => 32
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertNotEmpty($content->errors->children->rating->errors);

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }


    /**
     * Tests addition of a second comment to a review : check thread behaviour
     *
     * @param array $ids
     * @return mixed
     * @depends testAddValidCommentToReview
     */
    public function testAddValidSecondCommentToReview(array $ids)
    {
        try {


            $thread = $ids['thread'];
            $previousRating = 3;

            $body = "Je n'aime pas du tout.";
            $rating = 1;
            $headers = serialize(array("HEADER" => "HEADERDETEST2", "Test2" => "header de test2"));
            $cookie = "la_cfb_user=ee9bdf0dc9920a103190eb0fcb4c5c5ce08f3bc4; expires=Tue, 27-Feb-2024 13:47:30 GMT; path=/; domain=ladtech.fr";
            $userId = 2;

            $this->client->request('POST', "/api/threads/$thread/comments", array(
                'la_comment_comment' => array(
                    'body' => $body,
                    'headers' => $headers,
                    'rating' => $rating,
                    'cookie' => $cookie,
                    'userId' => $userId
                ),
            ));
            $content = json_decode($this->client->getResponse()->getContent());
            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
            $this->assertFalse(is_null($content));
            $this->assertEquals($body, $content->body);
            $this->assertEquals($headers, $content->headers);
            $this->assertEquals($rating, $content->rating);
            $this->assertEquals($cookie, $content->cookie);
            $this->assertEquals($userId, $content->user_id);

            $this->assertEquals(($rating + $previousRating) / 2, $content->thread->rating);
            $this->assertEquals(($rating + $previousRating), $content->thread->rating_sum);
            $this->assertEquals(2, $content->thread->rating_count);

            return $thread;

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }


    /**
     * Test sort comments by score (votes).
     *
     * @param $id
     * @return mixed
     * @depends testAddValidSecondCommentToReview
     */
    public function testGetCommentFlatSortedByRating($id)
    {


        $this->client->request('GET', "/api/threads/$id/comments?view=flat&sorter=rating_desc");
        $content = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content));
        $this->assertEquals(2, count($content->comments));

        $this->client->request('GET', "/api/threads/$id/comments?view=flat&sorter=rating_asc");
        $content2 = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertFalse(is_null($content2));
        $this->assertEquals(2, count($content->comments));

        $this->assertEquals($content->comments[0]->comment->id, $content2->comments[1]->comment->id);
        $this->assertEquals($content->comments[1]->comment->id, $content2->comments[0]->comment->id);

        return array(
            'thread' => $id,
            'comments' => array(
                $content->comments[0]->comment->id,
                $content->comments[1]->comment->id
            )
        );
    }


    /**
     * Cleanup of created entities
     *
     * @param array $ids
     * @depends testGetCommentFlatSortedByRating
     */
    public function testReviewCleanup(array $ids)
    {
        try {


            /** @var \Doctrine\ORM\EntityManager $em */
            $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');

            foreach ($ids['comments'] as $comment) {
                $em->remove($em->find("La\\CommentBundle\\Entity\\Comment", $comment));
            }
            $em->remove($em->find("La\\CommentBundle\\Entity\\Thread", $ids['thread']));

            $em->flush();

        } catch (\Exception $e) {
            $this->markTestIncomplete($e->getMessage());
        }
    }

}
