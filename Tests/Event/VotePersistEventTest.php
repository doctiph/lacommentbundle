<?php

namespace La\CommentBundle\Tests\Event;

use La\CommentBundle\Event\VotePersistEvent;

class VotePersistEventTest extends \PHPUnit_Framework_TestCase
{
    public function testAbortingPersistence()
    {


        $vote = $this->getMock('La\CommentBundle\Model\VoteInterface');
        $event = new VotePersistEvent($vote);
        $this->assertFalse($event->isPersistenceAborted());
        $event->abortPersistence();
        $this->assertTrue($event->isPersistenceAborted());
    }
}
