<?php

namespace La\CommentBundle\Tests\Event;

use La\CommentBundle\Event\CommentPersistEvent;

class CommentPersistEventTest extends \PHPUnit_Framework_TestCase
{
    public function testAbortingPersistence()
    {


        $comment = $this->getMock('La\CommentBundle\Model\CommentInterface');
        $event = new CommentPersistEvent($comment);
        $this->assertFalse($event->isPersistenceAborted());
        $event->abortPersistence();
        $this->assertTrue($event->isPersistenceAborted());
    }
}
