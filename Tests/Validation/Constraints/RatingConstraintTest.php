<?php
namespace La\CommentBundle\Tests\Validation\Constraints;

use La\CommentBundle\Validation\Constraints\RatingConstraint;
use La\CommentBundle\Validation\Constraints\RatingConstraintValidator;
use La\CommentBundle\Entity\Thread;
use La\CommentBundle\Entity\Comment;


class RatingConstraintTest extends \PHPUnit_Framework_TestCase
{
    public function testRatingConstraint()
    {


        $constraint = new RatingConstraint();
        $validator = new RatingConstraintValidator();

        $comment = new Comment();
        $thread = new Thread();
        $thread->setType($thread::REVIEW_TYPE);
        $comment->setThread($thread);

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext', array('addViolationAt'))->disableOriginalConstructor()->getMock();
        $validator->initialize($context);
        $context->expects($this->atLeastOnce())->method('addViolationAt')->with($this->equalTo($constraint->errorPath), $this->equalTo($constraint->message['blank']));
        $comment->setRating(null);
        $validator->validate($comment, $constraint);

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext', array('addViolationAt'))->disableOriginalConstructor()->getMock();
        $validator->initialize($context);
        $context->expects($this->atLeastOnce())->method('addViolationAt')->with($this->equalTo($constraint->errorPath), $this->equalTo($constraint->message['outofbounds']));
        $comment->setRating(133);
        $validator->validate($comment, $constraint);

        $thread->setType($thread::THREAD_TYPE);
        $comment->setThread($thread);
        $validator->validate($comment, $constraint);
        $this->assertEquals(0, $comment->getRating());

    }
}
