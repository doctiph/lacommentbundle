<?php

namespace La\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use La\CommentBundle\Model\Ban as BaseBan;


/**
 * @ORM\Table(
 *      name="lacomment__ban"
 * )
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Ban extends BaseBan
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;
}
