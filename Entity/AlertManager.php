<?php



namespace La\CommentBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use La\CommentBundle\Model\CommentManager as BaseCommentManager;
use La\CommentBundle\Model\ThreadInterface;
use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Sorting\SortingFactory;
use La\CommentBundle\Sorting\SortingInterface;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default ORM CommentManager.
 *
 */
class AlertManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var boolean
     */
    protected $allowRepliesOnReviews;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \La\CommentBundle\Sorting\SortingFactory $factory
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EventDispatcherInterface $dispatcher, SortingFactory $factory, EntityManager $em)
    {

        $this->em = $em;
        $this->repository = $em->getRepository('LaCommentBundle:Alert');
        $this->sortingFactory = $factory;

    }

    /**
     * @param Alert $alert
     */
    public function createAndSaveAlert(Alert $alert)
    {
        $alert->setStatus(Alert::STATUS_PENDING);
        $this->saveAlert($alert);
    }

    /**
     * @param Alert $alert
     */
    public function solveAndSaveAlert(Alert $alert, $moderator)
    {
        $alert->setModerator($moderator);
        $alert->setStatus(Alert::STATUS_SOLVED);
        $this->saveAlert($alert);
    }

    /**
     * @param Alert $alert
     */
    public function rejectAndSaveAlert(Alert $alert, $moderator)
    {
        $alert->setModerator($moderator);
        $alert->setStatus(Alert::STATUS_REJECTED);
        $this->saveAlert($alert);
    }

    /**
     * @param Alert $alert
     */
    public function saveAlert(Alert $alert)
    {
        $this->em->persist($alert);
        $this->em->flush();
    }

    public function getAlerts($offset = 0, $limit = null, $sorter = null)
    {
        $sorter = $this->sortingFactory->getSorter($sorter);

        $qb = $this->repository->createQueryBuilder('a');
        $res = $qb->select('a')
            ->innerJoin('a.comment', 'c', Expr\Join::WITH, 'c.visibility = 1')
            ->andWhere('a.status = 0 OR a.status = 1')
            ->orderBy('a.created', $sorter->getOrder())
            ->groupBy('a.comment')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        $comments = [];
        if (count($res) === 0) {
            return array();
        }
        foreach ($res as $alert) {
            $comments[] = $alert->getComment()->getId();
        }

        $qb = $this->repository->createQueryBuilder('a');
        $res = $qb->select('a')
            ->Where($qb->expr()->in('a.comment', $comments))
            ->orderBy('a.created', $sorter->getOrder())
            ->getQuery()
            ->getResult();

        $result = array();

        foreach ($res as $alert) {
            if (!isset($result[$alert->getComment()->getId()])) {
                $result[$alert->getComment()->getId()] = [];
            }
            if (!isset($result[$alert->getComment()->getId()]['alerts'])) {
                $result[$alert->getComment()->getId()]['alerts'] = [];
            }
            $result[$alert->getComment()->getId()]['comment'] = $alert->getComment();
            $result[$alert->getComment()->getId()]['alerts'][] = $alert;
        }
        return $result;

    }

    public function countAlerts()
    {
        $qb = $this->repository->createQueryBuilder('a');
        try {
            $res = $qb->select('a')
                ->where('a.status = 0')
                ->orderBy('a.created', 'DESC')
                ->groupBy('a.comment')
                ->getQuery()
                ->getResult();

            return count($res);
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * Find one alerts by its ID
     *
     * @param $id
     * @return Alert or null
     */
    public function findAlertById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    public function getSerializedHeaders(Request $request)
    {
        $validHeaders = array();

        foreach ($request->server->all() as $key => $header) {
            if (substr($key, 0, 5) === 'HTTP_' || $key == 'REMOTE_ADDR' || $key == 'PHP_SELF') {
                $validHeaders[$key] = $header;
            }
        }
        return @serialize($validHeaders);
    }
}
