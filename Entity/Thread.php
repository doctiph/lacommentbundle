<?php

namespace La\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use La\CommentBundle\Model\Thread as BaseThread;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="lacomment__thread"
 * )
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Thread extends BaseThread
{

}