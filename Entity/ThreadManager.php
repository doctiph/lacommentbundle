<?php



namespace La\CommentBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use La\CommentBundle\Model\ThreadInterface;
use La\CommentBundle\Model\ThreadManager as BaseThreadManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Default ORM ThreadManager.
 *
 */
class ThreadManager extends BaseThreadManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \Doctrine\ORM\EntityManager $em
     * @param string $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, EntityManager $em, $class)
    {
        parent::__construct($dispatcher);

        $this->em = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * Finds one comment thread by the given criteria
     *
     * @param  array $criteria
     * @return ThreadInterface
     */
    public function findThreadBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function findThreadsBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * Finds all threads.
     *
     * @return array of ThreadInterface
     */
    public function findAllThreads()
    {
        return $this->repository->findAll();
    }

    /**
     * Finds all threads, returns an iterable result
     *
     * @return array of ThreadInterface
     */
    public function findAllThreadsIterable()
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('t')
            ->orderBy('t.created', 'ASC');

        return $qb->getQuery()->iterate(null, 5);
    }

    /**
     * @return mixed
     */
    public function findThreadsCount()
    {
        $qb = $this->repository->createQueryBuilder('t');

        $res = $qb->select('COUNT(t)')
            ->getQuery()
            ->getSingleScalarResult();

        return $res;
    }

    /**
     * Find last active threads, up to $count threads.
     *
     * @param $count
     * @return mixed
     */
    public function findLastActiveThreads($count)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('t')
            ->orderBy('t.lastCommentAt', 'DESC')
            ->setMaxResults($count);

        $threads = $qb
            ->getQuery()
            ->execute();

        return $threads;
    }

    /**
     * {@inheritDoc}
     */
    public function isNewThread(ThreadInterface $thread)
    {
        return !$this->em->getUnitOfWork()->isInIdentityMap($thread);
    }

    /**
     * Saves a thread
     *
     * @param ThreadInterface $thread
     */
    protected function doSaveThread(ThreadInterface $thread)
    {
        $this->em->persist($thread);
        $this->em->flush();
    }

    /**
     * Returns the fully qualified comment thread class name
     *
     * @return string
     **/
    public function getClass()
    {
        return $this->class;
    }

    /**
     *  Has user subscribed to email notifications
     *
     * @param $userId
     * @param ThreadInterface $thread
     * @return bool
     */
    public function hasUserSubscribed(ThreadInterface $thread, $userId)
    {
        //@TODO
        return false;
    }


    /**
     * @param $obj
     */
    public function clear($obj)
    {
        $this->em->detach($obj);
        $this->em->clear();
    }

    /**
     *
     */
    public function disableLogger()
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

}
