<?php



namespace La\CommentBundle\Entity;

use Doctrine\ORM\EntityManager;
use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\VoteInterface;
use La\CommentBundle\Model\VoteManager as BaseVoteManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Default ORM VoteManager.
 *
 */
class VoteManager extends BaseVoteManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \Doctrine\ORM\EntityManager $em
     * @param $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, EntityManager $em, $class)
    {
        parent::__construct($dispatcher);

        $this->em = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * Persists a vote.
     *
     * @param \La\CommentBundle\Model\VoteInterface $vote
     */
    protected function doSaveVote(VoteInterface $vote)
    {
        $this->em->persist($vote->getComment());
        $this->em->persist($vote);
        $this->em->flush();
    }

    /**
     * Finds a vote by specified criteria.
     *
     * @param  array $criteria
     * @return VoteInterface
     */
    public function findVoteBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Finds all votes belonging to a comment.
     *
     * @param  \La\CommentBundle\Model\CommentInterface $comment
     * @return array|null
     */
    public function findVotesByComment(CommentInterface $comment)
    {
        $qb = $this->repository->createQueryBuilder('v');
        $qb->where('v.comment = :commentId');
        $qb->setParameter('commentId', $comment->getId());

        $votes = $qb->getQuery()->execute();

        return $votes;
    }

    /**
     * Finds a vote by comment and author
     *
     * @param CommentInterface $comment
     * @param $userId
     * @return VoteInterface|void
     */
    public function findVoteByCommentAndAuthor(CommentInterface $comment, $userId)
    {
        return $this->repository->findOneBy(array('comment' => $comment->getId(), 'user' => $userId));
    }

    public function hasUserVotedOnComment($userId, $comment)
    {
        try {

            if (is_object($comment)) {
                $comment = $comment->getId();
            }

            $qb = $this->repository->createQueryBuilder('v');
            $qb->where('v.user = :userId');
            $qb->andWhere('v.comment = :commentId');
            $qb->setParameter('userId', $userId);
            $qb->setParameter('commentId', $comment);

            return $qb->getQuery()->execute();

        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Returns the fully qualified comment vote class name
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

}
