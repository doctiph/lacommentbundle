<?php

namespace La\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *      name="lacomment__alert"
 * )
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Alert
{

    const STATUS_PENDING = 0;
    const STATUS_SOLVED = 1;
    const STATUS_REJECTED = 2;

    /**
     * @var integer $id
     *
     * @ORM\Id
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $created;
    /**
     * @var integer
     */
    protected $status;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $ip;
    /**
     * @var string
     */
    protected $headers;
    /**
     * @var string
     */
    protected $moderator;
    /**
     * @var Comment
     */
    protected $comment;


    /**
     * @param Comment $comment
     * @throws \Exception
     */
    public function __construct(Comment $comment)
    {
        if (is_null($comment)) {
            throw new \Exception("An alert must refer to an existing commentary");
        }
        $this->comment = $comment;
        $this->created = new \DateTime();
    }


    /**
     * @return \La\CommentBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        $this->setIp();
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function setIp()
    {
        if ($this->headers != '') {
            $headers = unserialize($this->headers);

            if (isset($headers['HTTP_TRUE_CLIENT_IP'])) {
                $this->ip = $headers['HTTP_TRUE_CLIENT_IP'];
            } elseif (isset($headers['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $headers['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($headers['REMOTE_ADDR'])) {
                $this->ip = $headers['REMOTE_ADDR'];
            }
        }
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $moderator
     */
    public function setModerator($moderator)
    {
        $this->moderator = $moderator;
    }

    /**
     * @return string
     */
    public function getModerator()
    {
        return $this->moderator;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

}
