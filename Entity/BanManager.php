<?php



namespace La\CommentBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use La\CommentBundle\Model\BanInterface;
use La\CommentBundle\Model\BanManager as BaseBanManager;

/**
 * Default ORM BanManager.
 */
class BanManager extends BaseBanManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Constructor.
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param $class
     */
    public function __construct(EntityManager $em, $class)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * Persists a ban.
     *
     * @param \La\CommentBundle\Model\BanInterface $ban
     */
    protected function doSaveBan(BanInterface $ban)
    {
        $this->em->persist($ban);
        $this->em->flush();
    }

    /**
     * Finds a ban by specified criteria.
     *
     * @param  array $criteria
     * @return BanInterface
     */
    public function findBanBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

}
