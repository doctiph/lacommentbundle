<?php

namespace La\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use La\CommentBundle\Model\Vote as BaseVote;


/**
 * @ORM\Table(
 *      name="lacomment__vote"
 * )
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Vote extends BaseVote
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;
}
