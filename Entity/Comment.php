<?php

namespace La\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use La\CommentBundle\Model\Comment as AbstractComment;
use La\CommentBundle\Validation\Constraints\RatingConstraint;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="lacomment__comment"
 * )
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Comment extends AbstractComment
{
    /**
     * All ancestors of the comment
     *
     * @var string
     */
    protected $ancestors = '';

    /**
     * @return array
     */
    public function getAncestors()
    {
        return $this->ancestors ? explode('/', $this->ancestors) : array();
    }

    /**
     * @param  array
     * @return null
     */
    public function setAncestors(array $ancestors)
    {
        $this->ancestors = implode('/', $ancestors);
        $this->depth = count($ancestors);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new RatingConstraint());
    }

}
