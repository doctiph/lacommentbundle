<?php



namespace La\CommentBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use La\CommentBundle\Model\CommentManager as BaseCommentManager;
use La\CommentBundle\Model\ThreadInterface;
use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Sorting\SortingFactory;
use La\CommentBundle\Sorting\SortingInterface;

/**
 * Default ORM CommentManager.
 *
 */
class CommentManager extends BaseCommentManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var boolean
     */
    protected $allowRepliesOnReviews;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \La\CommentBundle\Sorting\SortingFactory $factory
     * @param \Doctrine\ORM\EntityManager $em
     * @param $aPrioriModeration
     * @param $allowRepliesOnReviews
     * @param $repliesSorterAlias
     * @param string $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, SortingFactory $factory, EntityManager $em, $aPrioriModeration, $allowRepliesOnReviews, $repliesSorterAlias, $class)
    {
        parent::__construct($dispatcher, $factory, $aPrioriModeration);

        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $this->allowRepliesOnReviews = $allowRepliesOnReviews;
        $this->repliesSorterAlias = $repliesSorterAlias;

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * Returns a flat array of comments of a specific thread.
     *
     * @param  ThreadInterface $thread
     * @param  integer $depth
     * @param null $sorterAlias
     * @param integer $offset
     * @param integer $limit
     * @param bool $onlyVisible
     * @return array           of ThreadInterface
     */
    public function findCommentsByThread(ThreadInterface $thread, $depth = null, $sorterAlias = null, $offset = null, $limit = null, $onlyVisible = true)
    {
        /** @var SortingInterface $sorter */
        $sorter = $this->sortingFactory->getSorter($sorterAlias);
        $property = sprintf("c.%s", $sorter->getProperty());

        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->join('c.thread', 't')
            ->where('t.id = :thread')
            ->andWhere('c.depth = :depth')
            ->setParameter('thread', $thread->getId())
            ->setParameter('depth', 0)
            ->orderBy($property, $sorter->getOrder());

        if (!is_null($offset)) {
            $qb->setFirstResult($offset);
        }

        if (!is_null($limit)) {
            $qb->setMaxResults($limit);
        }

        if ($onlyVisible) {
            $qb->andWhere('c.visibility = true');
        }

        $comments = $qb->getQuery()->execute();

        /* @todo : vérifier si utile */
        $sorter = $this->sortingFactory->getSorter($sorterAlias);
        $comments = $sorter->sortFlat($comments);

        if (count($comments) > 0) {
            $mainCommentsIds = array();
            foreach ($comments as $mainComment) {
                $mainCommentsIds[] = $mainComment->getId();
            }

            $qb = $this->repository
                ->createQueryBuilder('c')
                ->where('c.ancestors IN (:mainCommentsIds)')
                ->orderBy('c.ancestors', 'ASC')
                ->setParameter('mainCommentsIds', $mainCommentsIds);

            if ($onlyVisible) {
                $qb->andWhere('c.visibility = true');
            }

            if (null !== $depth && $depth >= 0) {
                // Queries for an additional level so templates can determine
                // if the final 'depth' layer has children.

                $qb->andWhere('c.depth < :depth')
                    ->setParameter('depth', $depth + 1);
            }

            $subComments = $qb->getQuery()->execute();

            is_null($this->repliesSorterAlias) ? : $sorter = $this->sortingFactory->getSorter($this->repliesSorterAlias);
            $subComments = $sorter->sortFlat($subComments);

            $comments = array_merge($comments, $subComments);
        }

        return $comments;
    }

    /**
     * @param $limit
     * @param null $moderationState
     * @param null $sorter
     * @return mixed|void
     */
    public function findLastComments($offset = 0, $limit = null, $moderationState = null, $sorter = null)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->orderBy('c.ancestors', 'ASC');

        if (null !== $limit) {
            $qb->setFirstResult($offset);
            $qb->setMaxResults($limit);
        }

        if (null !== $moderationState) {
            $qb->andWhere('c.moderationState = :moderationState')
                ->setParameter('moderationState', $moderationState);
        }

        $comments = $qb
            ->getQuery()
            ->execute();

        /** @var SortingInterface $sorter */
        $sorter = $this->sortingFactory->getSorter($sorter);
        $comments = $sorter->sortFlat($comments);

        return $comments;
    }

    protected function setCommentsFromCriteriaQb(&$qb, $data)
    {

        // state
        $moderationState = null;
        if (isset($data['state'])) {
            if ($data['state'] === 'nop') {
                $qb->Where('c.moderationState = 1 OR c.moderationState = 0');
            } elseif ($data['state'] === 'ok') {
                $qb->Where('c.moderationState = :moderationState')
                    ->setParameter('moderationState', CommentInterface::STATE_VALIDATED);
            } else {
                $qb->Where('c.moderationState NOT in (0,1, :moderationState)')
                    ->setParameter('moderationState', CommentInterface::STATE_VALIDATED);
            }
        }

        //id
        if (isset($data['id']) and !empty($data['id'])) {
            $qb->andWhere('c.id = :id_comment ')
                ->setParameter('id_comment', $data['id']);
        }

        //commentaire
        if (isset($data['comment']) and !empty($data['comment'])) {
            $qb->andWhere('c.body LIKE :body ')
                ->setParameter('body', '%' . $data['comment'] . '%');
        }

        //title
        if (isset($data['title']) and !empty($data['title'])) {
            $qb->andWhere('c.title LIKE :body ')
                ->setParameter('body', '%' . $data['title'] . '%');
        }

        //username
        if (isset($data['username']) and !empty($data['username'])) {
            $qb->andWhere('c.username = :username ')
                ->setParameter('username', $data['username']);
        }

        //user_id
        if (isset($data['user_id']) and !empty($data['user_id'])) {
            $qb->andWhere('c.userId = :user_id ')
                ->setParameter('user_id', $data['user_id']);
        }

        //ip
        if (isset($data['ip']) and !empty($data['ip'])) {
            $qb->andWhere('c.ip LIKE :ip ')
                ->setParameter('ip', '%' . $data['ip'] . '%');
        }

        //cause
        if (isset($data['moderation_cause']) and !empty($data['moderation_cause'])) {
            $qb->andWhere('c.moderationCause = :moderation_cause ')
                ->setParameter('moderation_cause', $data['moderation_cause']);
        }

    }

    public function getCommentsFromCriteria($data, $offset = 0, $limit = null, $sorter = null)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository->createQueryBuilder('c');

        $this->setCommentsFromCriteriaQb($qb, $data);

        $qb->orderBy('c.ancestors', 'ASC');

        if (null !== $limit) {
            $qb->setFirstResult($offset);
            $qb->setMaxResults($limit);
        }

        $comments = $qb
            ->getQuery()
            ->execute();
        /** @var SortingInterface $sorter */
        $sorter = $this->sortingFactory->getSorter($sorter);
        $comments = $sorter->sortFlat($comments);

        return $comments;
    }

    public function getCountCommentsFromCriteria($data)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('c')->select('COUNT(c)');

        $this->setCommentsFromCriteriaQb($qb, $data);

        $res = $qb
            ->getQuery()
            ->getSingleScalarResult();
        return $res;
    }

    /**
     * @return mixed
     */
    public function countPendingComments()
    {
        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->select('COUNT(c)')
            ->where('c.moderationState = :moderationState')
            ->setParameter('moderationState', CommentInterface::STATE_PENDING)
            ->getQuery()
            ->getSingleScalarResult();

        return $res;
    }

    /**
     * @return mixed
     */
    public function countModeratedComments()
    {
        $qb = $this->repository->createQueryBuilder('c');
        $date = new \DateTime();
        $date->modify('-1 year');

        $res = $qb->select('COUNT(c)')
            ->where('c.moderationState NOT IN (0 , 1) AND c.createdAt > :oneyear')
            ->setParameter('oneyear', $date->format('Y-M-D'))
            ->getQuery()
            ->getSingleScalarResult();

        return $res;
    }

    /**
     * @return mixed
     */
    public function countRefusedComments()
    {
        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->select('COUNT(c)')
            ->where('c.moderationState = :moderationState')
            ->setParameter('moderationState', CommentInterface::STATE_DELETED)
            ->getQuery()
            ->getSingleScalarResult();

        return $res;
    }

    /**
     * @param int $offset
     * @param null $limit
     * @param null $sorterAlias
     * @return mixed
     */
    public function findLastModeratedComments($offset = 0, $limit = null, $sorterAlias = null)
    {
        /** @var SortingInterface $sorter */
        $sorter = $this->sortingFactory->getSorter($sorterAlias);
        $date = new \DateTime();
        $date->modify('-1 year');

        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->where('c.moderationState NOT IN (0 , 1) AND c.createdAt > :oneyear')
            ->setParameter('oneyear', $date->format('Y-M-D'))
            ->orderBy('c.moderatedOn', $sorter->getOrder())
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $comments = $qb
            ->getQuery()
            ->execute();

        return $comments;
    }

    /**
     * @param int $offset
     * @param null $limit
     * @param null $sorterAlias
     * @return mixed
     */
    public function findRefusedComments($offset = 0, $limit = null, $sorterAlias = null)
    {
        /** @var SortingInterface $sorter */
        $sorter = $this->sortingFactory->getSorter($sorterAlias);

        /** @var QueryBuilder $qb */
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->where('c.moderationState IN (:moderationState)')
            ->setParameter('moderationState', CommentInterface::STATE_DELETED)
            ->orderBy('c.moderatedOn', $sorter->getOrder())
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $comments = $qb
            ->getQuery()
            ->execute();

        return $comments;
    }

    /**
     * Returns the requested comment tree branch
     *
     * @param  integer $commentId
     * @param  string $sorter
     * @return array   See findCommentTreeByThread
     */
    public function findCommentTreeByCommentId($commentId, $sorter = null)
    {
        $qb = $this->repository->createQueryBuilder('c');
        $qb->join('c.thread', 't')
            ->where('LOCATE(:path, CONCAT(\'/\', CONCAT(c.ancestors, \'/\'))) > 0')
            ->orderBy('c.ancestors', 'ASC')
            ->setParameter('path', "/{$commentId}/");

        $comments = $qb->getQuery()->execute();

        if (!$comments) {
            return array();
        }

        $sorter = $this->sortingFactory->getSorter($sorter);

        $trimParents = current($comments)->getAncestors();

        return $this->organiseComments($comments, $sorter, $trimParents);
    }

    /**
     * Performs persisting of the comment.
     *
     * @param CommentInterface $comment
     */
    protected function doSaveComment(CommentInterface $comment)
    {
        $this->em->persist($comment->getThread());
        $this->em->persist($comment);
        $this->em->flush();
    }

    protected function doSaveThread(ThreadInterface $thread)
    {
        $this->em->persist($thread);
        $this->em->flush();
    }

    /**
     * Find one comment by its ID
     *
     * @param $id
     * @return Comment or null
     */
    public function findCommentById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function isNewComment(CommentInterface $comment)
    {
        return !$this->em->getUnitOfWork()->isInIdentityMap($comment);
    }

    /**
     * Returns the fully qualified comment thread class name
     *
     * @return string
     **/
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param ThreadInterface $thread
     * @param $moderationState
     * @return mixed
     */
    public function findCommentsByThreadAndModerationState(ThreadInterface $thread, $moderationState)
    {

        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->where('c.thread = :thread')
            ->andWhere('c.moderationState IN (:state)')
            ->setParameter('thread', $thread->getId())
            ->setParameter('state', $moderationState)
            ->getQuery()
            ->getResult();

        return $res;
    }

    /**
     * @param ThreadInterface $thread
     * @return mixed
     */
    public function countVisibleCommentsForThread(ThreadInterface $thread)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->select('COUNT(c)')
            ->where('c.thread = :thread')
            ->andWhere('c.visibility = TRUE')
            ->setParameter('thread', $thread->getId())
            ->getQuery()
            ->getSingleScalarResult();

        return $res;
    }

    /**
     * Update thread rating
     *
     * @param ThreadInterface $thread
     * @return mixed|void
     */
    public function updateThreadRating(ThreadInterface $thread)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->repository->createQueryBuilder('c');

        $sum = $qb->select('SUM(c.rating)')
            ->where('c.thread = :thread')
            ->andWhere('c.visibility = TRUE')
            ->setParameter('thread', $thread->getId())
            ->getQuery()
            ->getSingleScalarResult();

        $count = $qb->select('COUNT(c.rating)')
            ->where('c.thread = :thread')
            ->andWhere('c.visibility = TRUE')
            ->setParameter('thread', $thread->getId())
            ->getQuery()
            ->getSingleScalarResult();

        $rating = $count != 0 ? ($sum / $count) : 0;

        $thread->setRating($rating);
        $thread->setRatingCount($count);
        $thread->setRatingSum($sum);

    }

    /**
     * @param ThreadInterface $thread
     * @return mixed
     */
    public function findLastCommentDate(ThreadInterface $thread)
    {
        $qb = $this->repository->createQueryBuilder('c');
        try {
            $res = $qb->select('c.createdAt')
                ->where('c.thread = :thread')
                ->andWhere('c.visibility = TRUE')
                ->orderBy('c.createdAt', 'DESC')
                ->setMaxResults(1)
                ->setParameter('thread', $thread->getId())
                ->getQuery()
                ->getSingleScalarResult();

            return new \DateTime($res);
        } catch (\Exception $e) {
            return null;
//            throw new \Exception("Cannot convert to DateTime : '%s' in %s, %s",$res, __FUNCTION__, __CLASS__);
        }
    }

    /**
     * @param array $ancestors
     * @param $moderationState
     * @return mixed
     */
    public function updateOffspringModerationState(array $ancestors, $moderationState)
    {
        $ancestorsString = implode('/', $ancestors);
        $ancestorsLikeString = implode('/', $ancestors) . '/%';

        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->update()
//            ->set('c.previous')
            ->set('c.moderationState', ':state')
            ->where('c.ancestors = :ancestors')
            ->orWhere('c.ancestors LIKE :ancestorslike')
            ->setParameter('state', $moderationState)
            ->setParameter('ancestors', $ancestorsString)
            ->setParameter('ancestorslike', $ancestorsLikeString)
            ->getQuery()
            ->execute();

        return $res;
    }

    /**
     * @param array $ancestors
     * @return mixed
     */
    public function getOffspringIds(array $ancestors)
    {
        $ancestorsString = implode('/', $ancestors);
        $ancestorsLikeString = implode('/', $ancestors) . '/%';

        $qb = $this->repository->createQueryBuilder('c');

        $res = $qb->select('c.id')
            ->where('c.ancestors = :ancestors')
            ->orWhere('c.ancestors LIKE :ancestorslike')
            ->setParameter('ancestors', $ancestorsString)
            ->setParameter('ancestorslike', $ancestorsLikeString)
            ->getQuery()
            ->getResult();

        return $res;
    }

    /**
     * @param $since
     * @return array
     */
    public function findModeratedThreads($since)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $then = new \DateTime();
        $then->modify(sprintf("-%d seconds", $since));

        $res = $qb->select('IDENTITY(c.thread) AS id', 'c.moderatedOn')
            ->where('c.moderatedOn IS NOT NULL')
            ->andWhere('c.moderatedOn > :then')
            ->setParameter('then', $then)
            ->groupBy('c.thread')
            ->getQuery()
            ->getResult();

        return $res;
    }

    ///////////////////// USERS /////////////////////////

    /**
     * @param ThreadInterface $thread
     * @param bool $hasRated
     * @param bool $excludeAnswers
     * @return array
     */
    public function findAllCommentsAuthors(ThreadInterface $thread, $hasRated = false, $excludeAnswers = false)
    {
        try {

            $qb = $this->repository
                ->createQueryBuilder('c')
                ->join('c.thread', 't')
                ->where('t.id = :thread')
                ->orderBy('c.ancestors', 'ASC')
                ->setParameter('thread', $thread->getId());

            if ($hasRated) {
                $qb->andWhere('c.rating > 0');
            }

            if ($excludeAnswers) {
                $qb->andWhere('c.depth = 0');
            }

            $comments = $qb
                ->getQuery()
                ->execute();

            return $comments;
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @param ThreadInterface $thread
     * @param $userId
     * @param bool $excludeAnswers
     * @return array|bool
     */
    public function hasUserPosted(ThreadInterface $thread, $userId, $excludeAnswers = false)
    {

        $comments = $this->findAllCommentsAuthors($thread, false, $excludeAnswers);
        $userComments = array();

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            if ($comment->getUserId() == $userId) {
                $userComments[] = $comment->getId();
            }
        }

        if (empty($userComments)) {
            return false;
        } else {
            return $userComments;
        }

    }

    /**
     * @param ThreadInterface $thread
     * @param $userId
     * @return array|bool
     */
    public function hasUserRated(ThreadInterface $thread, $userId)
    {
        $comments = $this->findAllCommentsAuthors($thread, true);
        $userComments = array();

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            if ($comment->getUserId() == $userId) {
                $userComments[] = $comment->getId();
            }
        }

        if (empty($userComments)) {
            return false;
        } else {
            return $userComments;
        }
    }

    /**
     * @param ThreadInterface $thread
     * @param $userId
     * @param VoteManager $voteManager
     * @return array|bool
     */
    public function hasUserVoted(ThreadInterface $thread, $userId, $voteManager)
    {
        $comments = $this->findAllCommentsAuthors($thread);
        $votedComments = array();

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            $res = $voteManager->hasUserVotedOnComment($userId, $comment->getId());
            if (!empty($res)) {
                $votedComments[] = array($res[0]->getComment() => $res[0]->getValue());
            }
        }

        if (empty($votedComments)) {
            return false;
        } else {
            return $votedComments;
        }
    }

    public function isUserAllowedToRate(ThreadInterface $thread, $userId)
    {
        if ($this->hasUserRated($thread, $userId)) {
            return false;
        } else {
            return true;
        }
    }

    public function isUserAllowedToPost(Thread $thread, $userId)
    {
        // Banned used
        if ($this->isUserBanned($thread, $userId)) {
            return false;
        }

        // Review allows only one post by user
        if (($thread->getType() == $thread::REVIEW_TYPE) && ($this->hasUserPosted($thread, $userId, true))) {
            return false;
        }

        return true;
    }

    public function isUserAllowedtoAnswer(Thread $thread, $userId)
    {
        if ($thread->getType() == $thread::REVIEW_TYPE) {
            return (true && $this->allowRepliesOnReviews);
        }
        return true;
    }

    public function isUserBanned(ThreadInterface $thread, $userId)
    {
        //@TODO
        return false;
    }
}
