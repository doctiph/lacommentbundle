<?php



namespace La\CommentBundle\Event;

use La\CommentBundle\Model\ThreadInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * An event that occurs related to a thread.
 *
 */
class ThreadEvent extends Event
{
    private $thread;

    /**
     * Constructs an event.
     *
     * @param \La\CommentBundle\Model\ThreadInterface $thread
     */
    public function __construct(ThreadInterface $thread)
    {
        $this->thread = $thread;
    }

    /**
     * Returns the thread for this event.
     *
     * @return \La\CommentBundle\Model\ThreadInterface
     */
    public function getThread()
    {
        return $this->thread;
    }
}
