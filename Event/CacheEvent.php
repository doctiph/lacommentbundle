<?php

namespace La\CommentBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class CacheEvent extends Event
{
    protected $cacheFunction;
    protected $deleteByTagsFunction;
    protected $deleteByKeysFunction;


// Cache
    public function setCacheFunction(callable $cacheFunction)
    {
        $this->cacheFunction = $cacheFunction;
    }

    public function cache($arrayTags, $arrayKeys, callable $callback, $ttl)
    {
        if (!is_null($this->cacheFunction)) {
            $cacheFunction = $this->cacheFunction;
            return $cacheFunction($arrayTags, $arrayKeys, $callback, $ttl);
        } else {
            return $callback();
        }
    }


// Delete by tags
    public function setDeleteByTagsFunction(callable $deleteByTagsFunction)
    {
        $this->deleteByTagsFunction = $deleteByTagsFunction;
    }

    public function deleteByTags(array $arrayTags)
    {
        if (!is_null($this->deleteByTagsFunction)) {
            $deleteByTagsFunction = $this->deleteByTagsFunction;
            return $deleteByTagsFunction($arrayTags);
        }
        return null;
    }


// Delete by keys
    public function setDeleteByKeysFunction(callable $deleteByKeysFunction)
    {
        $this->deleteByKeysFunction = $deleteByKeysFunction;
    }

    public function deleteByKeys(array $arrayKeys)
    {
        if (!is_null($this->deleteByKeysFunction)) {
            $deleteByKeysFunction = $this->deleteByKeysFunction;
            return $deleteByKeysFunction($arrayKeys);
        }
        return null;
    }
}
