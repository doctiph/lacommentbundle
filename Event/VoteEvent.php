<?php



namespace La\CommentBundle\Event;

use La\CommentBundle\Model\VoteInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * An event that occurs related to a vote.
 *
 */
class VoteEvent extends Event
{
    private $vote;

    /**
     * Constructs an event.
     *
     * @param \La\CommentBundle\Model\VoteInterface $vote
     */
    public function __construct(VoteInterface $vote)
    {
        $this->vote = $vote;
    }

    /**
     * Returns the vote for the event.
     *
     * @return \La\CommentBundle\Model\VoteInterface
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set a new vote object for the event
     *
     * @param VoteInterface $vote
     */
    public function setVote(VoteInterface $vote){
        $this->vote = $vote;
    }
}
