<?php



namespace La\CommentBundle\Event;

use La\CommentBundle\Model\CommentInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * An event that occurs related to a comment.
 *
 */
class CommentEvent extends Event
{
    private $comment;

    /**
     * Constructs an event.
     *
     * @param \La\CommentBundle\Model\CommentInterface $comment
     */
    public function __construct(CommentInterface $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Returns the comment for this event.
     *
     * @return \La\CommentBundle\Model\CommentInterface
     */
    public function getComment()
    {
        return $this->comment;
    }
}
