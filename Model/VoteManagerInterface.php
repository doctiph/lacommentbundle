<?php



namespace La\CommentBundle\Model;

/**
 * Manages voting scores for comments.
 *
 */
interface VoteManagerInterface
{
    /**
     * Returns the class of the Vote object.
     *
     * @return string
     */
    public function getClass();

    /**
     * Creates a Vote object.
     *
     * @param  CommentInterface $comment
     * @return VoteInterface
     */
    public function createVote(CommentInterface $comment);

    /**
     * Persists a vote.
     *
     * @param  VoteInterface $vote
     * @return void
     */
    public function saveVote(VoteInterface $vote);

    /**
     * Finds a vote by specified criteria.
     *
     * @param  array         $criteria
     * @return VoteInterface
     */
    public function findVoteBy(array $criteria);

    /**
     * Finds a vote by id.
     *
     * @param  $id
     * @return VoteInterface
     */
    public function findVoteById($id);

    /**
     * Finds all votes for a comment.
     *
     * @param  CommentInterface $comment
     * @return array of VoteInterface
     */
    public function findVotesByComment(CommentInterface $comment);

    /**
     * Find a vote by comment and author
     *
     * @param CommentInterface $comment
     * @param $userId
     * @return VoteInterface
     */
    public function findVoteByCommentAndAuthor(CommentInterface $comment, $userId);

}
