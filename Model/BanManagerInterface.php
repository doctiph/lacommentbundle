<?php

namespace La\CommentBundle\Model;

interface BanManagerInterface
{

    function findBanById($id);
    function findBanBy(array $criteria);
    function createBan(CommentInterface $comment);
    function saveBan(BanInterface $ban);

}
