<?php

namespace La\CommentBundle\Model;

use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\CommentManagerInterface;
use La\CommentBundle\Model\BanManagerInterface;

/**
 * Moderation manager called by specific moderation bundles
 *
 */
class ModerationManager implements ModerationManagerInterface
{

    /**
     * @var CommentManagerInterface
     */
    protected $commentManager;

    /**
     * @var BanManagerInterface
     */
    protected $banManager;

    /**
     * @param CommentManagerInterface $commentManager
     * @param BanManagerInterface $banManager
     */
    public function __construct(CommentManagerInterface $commentManager, BanManagerInterface $banManager)
    {
        $this->commentManager = $commentManager;
        $this->banManager = $banManager;
    }


    /**
     * return size of pending comments waiting to be moderated
     */
    public function countPendingComments()
    {
        return $this->commentManager->countPendingComments();
    }

    /**
     * return pending comments waiting to be moderated
     *
     * @param $count
     * @param string $sorter
     * @return mixed
     */
    public function findLastPendingComments($offset = 0, $limit = null, $sorter = 'date_asc')
    {
        return $this->commentManager->findLastComments(
            $offset,
            $limit,
            CommentInterface::STATE_PENDING,
            $sorter
        );
    }

    public function countModeratedComments()
    {
        return $this->commentManager->countModeratedComments();
    }


    public function countRefusedComments()
    {
        return $this->commentManager->countRefusedComments();
    }


    public function findRecentlyModeratedComments($offset = 0, $limit = null, $sorterAlias = null)
    {
        return $this->commentManager->findLastModeratedComments($offset, $limit, $sorterAlias);
    }

    public function findRefusedComments($offset = 0, $limit = null, $sorterAlias = null)
    {
        return $this->commentManager->findRefusedComments($offset, $limit, $sorterAlias);
    }

    /**
     * Return a pending comment by its id or return null if no valid comment is found (i.e. invalid id, or invalid comment state)
     *
     * @param $id
     * @return CommentInterface|null
     */
    public function getValidPendingCommentById($id)
    {
        $comment = $this->commentManager->findCommentById($id);
        if (!$comment instanceof CommentInterface || $comment->getModerationState() != CommentInterface::STATE_PENDING) {
            return null;
        }
        return $comment;
    }

    /**
     * Return a comment being moderated by its id or return null if no valid comment is found (i.e. invalid id, or invalid comment state)
     *
     * @param $id
     * @return CommentInterface|null
     */
    public function getValidInProgressCommentById($id)
    {
        if (is_null($id)) {
            return null;
        }

        $comment = $this->commentManager->findCommentById($id);
        if (!$comment instanceof CommentInterface || $comment->getModerationState() != CommentInterface::STATE_IN_PROGRESS) {
            return null;
        }

        return $comment;
    }

    /**
     * set a comment to "in progress" and save it
     *
     * @param CommentInterface $comment
     * @return mixed
     */
    public function setInProgress(CommentInterface $comment)
    {
        $comment->setModerationState(CommentInterface::STATE_IN_PROGRESS);
        return $this->commentManager->saveComment($comment);
    }

    /**
     * Set a comment to "moderated" and save its moderation info.
     *
     * @param CommentInterface $comment
     * @param $state
     * @param $cause
     * @param null $moderator
     * @param null $date
     * @return mixed
     */
    public function setModerated(CommentInterface $comment, $state, $cause, $moderator = null, $date = null)
    {

        if ($state == static::MODERATION_OK) {
            $moderationState = CommentInterface::STATE_VALIDATED;
        } elseif ($state == static::MODERATION_NOT_OK) {
            $moderationState = CommentInterface::STATE_DELETED;
        } else {
            return false;
        }


        if (is_null($date)) {
            $date = new \DateTime();
        }
        $this->commentManager->setModerationState($comment, $moderationState);
        $comment->setModerationCause($cause);
        $comment->setModeratedBy($moderator);
        $comment->setModeratedOn($date);

        return $this->commentManager->saveComment($comment);
    }


    public function ban($commentId, $bannedUntil, $banMessage, $banAuthor = null)
    {
        // Process bans if necessary
        if (!is_null($bannedUntil)) {

            // Get comment
            $comment = $this->commentManager->findCommentById($commentId);
            if (!$comment instanceof CommentInterface || $comment->getModerationState() != CommentInterface::STATE_IN_PROGRESS) {
                return false;
            }

            // Then process ban object
            $ban_end_date = new \DateTime($bannedUntil);

            /** @var BanInterface $existing_ban */
            $existing_ban = $this->banManager->findBanBy(array('userId' => $comment->getUserId()));
            if (!is_null($existing_ban)) {
                if ($existing_ban->getBannedUntil()->diff($ban_end_date) == 1) { // diff = 1 means $ban_end_date is subsequent to registered ban date, so we extend the ban.
                    $existing_ban->setBannedUntil($ban_end_date);
                }
                $existing_ban->setBanCause($banMessage);
                $this->banManager->saveBan($existing_ban);
            } else {
                /** @var BanInterface $ban */
                $ban = $this->banManager->createBan($comment);
                $ban->setUserId($comment->getUserId());
                $ban->setUsername($comment->getUsername());
                $ban->setEmail($comment->getEmail());
                $ban->setIp($comment->getIp());
                $ban->setCookie($comment->getCookie());

                $ban->setBannedSince(new \DateTime());
                $ban->setBannedUntil($ban_end_date);
                $ban->setBanCause(urldecode($banMessage));
                $ban->setBannedBy($banAuthor);

                if ($this->banManager->saveBan($ban) == false) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getCommentsFromCriteria($data, $offset = 0, $limit = null, $sorter = null)
    {
        $empty = true;
        foreach ($data as $criteria) {
            if (!empty($criteria)) {
                $empty = false;
                break;
            }
        }
        if ($empty) {
            return null;
        }
        return $this->commentManager->getCommentsFromCriteria($data, $offset, $limit, $sorter);
    }

    public function getCountCommentsFromCriteria($data)
    {
        $empty = true;
        foreach ($data as $criteria) {
            if (!empty($criteria)) {
                $empty = false;
                break;
            }
        }
        if ($empty) {
            return null;
        }
        return $this->commentManager->getCountCommentsFromCriteria($data);
    }

    public function getModerationCauses()
    {
        return array(
            2 => "Agression",
            13 => "Appel au suicide",
            18 => "Attaque contre religion",
            12 => "Blague interdite",
            19 => "Copyright non respecté",
            1 => "Diffamation",
            5 => "Illégal",
            14 => "Illisible",
            3 => "Insulte",
            16 => "Manque de respect envers victime",
            10 => "Pédophilie",
            6 => "Pseudo insultant",
            9 => "Pub",
            17 => "Prosélytisme religieux",
            15 => "Prostitution",
            4 => "Racisme",
            11 => "Sexe, nu, érotisme, porno",
            8 => "SPAM",
            7 => "Vie privée",
            99 => "Autre motif"
        );
    }
}
