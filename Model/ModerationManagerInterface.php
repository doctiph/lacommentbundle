<?php

namespace La\CommentBundle\Model;

/**
 * Moderation manager Interface
 *
 */
interface ModerationManagerInterface
{

    const MODERATION_NOT_OK = 0;
    const MODERATION_OK = 1;

    /**
     * @param CommentManagerInterface $commentManager
     * @param BanManagerInterface $banManager
     */
    public function __construct(CommentManagerInterface $commentManager, BanManagerInterface $banManager);

    /**
     * @param int $offset
     * @param null $limit
     * @param string $sorter
     * @return mixed
     */
    public function findLastPendingComments($offset = 0, $limit = null, $sorter = 'date_asc');

    /**
     * Return a pending comment by its id or return null if no valid comment is found (i.e. invalid id, or invalid comment state)
     *
     * @param $id
     * @return CommentInterface|null
     */
    public function getValidPendingCommentById($id);

    /**
     * Return a comment being moderated by its id or return null if no valid comment is found (i.e. invalid id, or invalid comment state)
     *
     * @param $id
     * @return CommentInterface|null
     */
    public function getValidInProgressCommentById($id);

    /**
     * set a comment to "in progress" and save it
     *
     * @param CommentInterface $comment
     * @return mixed
     */
    public function setInProgress(CommentInterface $comment);

    /**
     * Set a comment to "moderated" and save its moderation info.
     *
     * @param CommentInterface $comment
     * @param $state
     * @param $cause
     * @param null $moderator
     * @param null $date
     * @return mixed
     */
    public function setModerated(CommentInterface $comment, $state, $cause, $moderator = null, $date = null);

}
