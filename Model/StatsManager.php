<?php

namespace La\CommentBundle\Model;

use La\StatsBundle\Model\AbstractStatsManager;
use La\CommentBundle\Model\CommentInterface;

/**
 * Class StatsManager
 * @package La\UserBundle\Services
 */
class StatsManager extends AbstractStatsManager
{
    /**
     * @return string
     */
    public function getType()
    {
        return "comment";
    }


    /**
     * @return array
     */
    public function listAvailableStats()
    {
        return array(
            "monthlyComments" => $this->getMonthlyCommentsByDay(),
            "yearlyComments" => $this->getYearlyCommentsByMonth(),
        );
    }

    /**
     * @return string
     */
    public function getMonthlyCommentsByDay()
    {
        $toSerialize = [];

        $q = "SELECT DATE(C.created_at) as 'date', count(C.id) as 'nb'
                  FROM lacomment__comment C
                  WHERE C.created_at >= '" . $this->oneMonthAgo . "'
                  %s
                  GROUP BY DAY(C.created_at), MONTH(C.created_at), YEAR(C.created_at) ORDER BY YEAR(C.created_at), MONTH(C.created_at), DAY(C.created_at)";
        // Nouveaux
        $query = sprintf($q, '');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Nouveaux',
            'value' => $stats
        );


        // Validés
        $query = sprintf($q, ' AND C.moderation_state = 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Validés',
            'value' => $stats
        );


        // Refusés
        $query = sprintf($q, ' AND C.moderation_state > 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Refusés',
            'value' => $stats
        );


        // En attente
        $query = sprintf($q, ' AND C.moderation_state < 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'En attente',
            'value' => $stats
        );

        return serialize($toSerialize);
    }

    /**
     * @return string
     */
    public function getYearlyCommentsByMonth()
    {
        $toSerialize = [];

        $q = "SELECT  DATE_FORMAT(C.created_at, '%%Y-%%m') as 'date', count(C.id) as 'nb'
                  FROM lacomment__comment C
                  WHERE C.created_at >= '" . $this->oneYearAgo . "'
                  %s
                  GROUP BY MONTH(C.created_at), YEAR(C.created_at) ORDER BY YEAR(C.created_at), MONTH(C.created_at)";

        // Nouveaux
        $query = sprintf($q, '');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Nouveaux',
            'value' => $stats
        );


        // Validés
        $query = sprintf($q, ' AND C.moderation_state = 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Validés',
            'value' => $stats
        );


        // Refusés
        $query = sprintf($q, ' AND C.moderation_state > 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'Refusés',
            'value' => $stats
        );


        // En attente
        $query = sprintf($q, ' AND C.moderation_state < 3');

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());

        $toSerialize[] = array(
            'name' => 'En attente',
            'value' => $stats
        );

        return serialize($toSerialize);
    }

}