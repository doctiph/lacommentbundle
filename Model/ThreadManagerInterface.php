<?php



namespace La\CommentBundle\Model;

/**
 * Interface to be implemented by comment thread managers. This adds an additional level
 * of abstraction between your application, and the actual repository.
 *
 * All changes to comment threads should happen through this interface.
 *
 */
interface ThreadManagerInterface
{
    /**
     * @param  string          $id
     * @return ThreadInterface
     */
    public function findThreadById($id);

    /**
     * Finds one comment thread by the given criteria
     *
     * @param  array           $criteria
     * @return ThreadInterface
     */
    public function findThreadBy(array $criteria);

    /**
     * Finds threads by the given criteria
     *
     * @param array $criteria
     *
     * @return array of ThreadInterface
     */
    public function findThreadsBy(array $criteria);

    /**
     * Finds all threads.
     *
     * @return array of ThreadInterface
     */
    public function findAllThreads();


    /**
     * Finds last active threads.
     *
     * @param $count
     * @return mixed
     */
    public function findLastActiveThreads($count);

    /**
     * Creates an empty comment thread instance
     *
     * @param  bool   $id
     * @return Thread
     */
    public function createThread($id = null);

    /**
     * Saves a thread
     *
     * @param ThreadInterface $thread
     */
    public function saveThread(ThreadInterface $thread);

    /**
     * Checks if the thread was already persisted before, or if it's a new one.
     *
     * @param ThreadInterface $thread
     *
     * @return boolean True, if it's a new thread
     */
    public function isNewThread(ThreadInterface $thread);

    /**
     * Returns the comment thread fully qualified class name
     *
     * @return string
     */
    public function getClass();
}
