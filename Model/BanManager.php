<?php

namespace La\CommentBundle\Model;

use La\CommentBundle\Entity\Ban as EntityBan;

abstract class BanManager implements BanManagerInterface
{
    /**
     * Finds a ban by id.
     *
     * @param  $id
     * @return BanInterface
     */
    public function findBanById($id)
    {
        return $this->findBanBy(array('id' => $id));
    }

    /**
     * Creates a Ban object.
     *
     * @param  CommentInterface $comment
     * @return BanInterface
     */
    public function createBan(CommentInterface $comment)
    {
        $ban = new EntityBan();
        $ban->setComment($comment);

        return $ban;
    }

    public function saveBan(BanInterface $ban)
    {
        if (null === $ban->getComment()) {
            throw new \InvalidArgumentException('Ban passed into saveBan must have a comment');
        }

        $this->doSaveBan($ban);
    }

    abstract protected function doSaveBan(BanInterface $ban);


}
