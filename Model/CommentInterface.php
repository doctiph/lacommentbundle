<?php



namespace La\CommentBundle\Model;

interface CommentInterface
{

    /**
     * Different comment moderation states
     *
     * # |        STATE     |   DESCRIPTION
     * -------------------------------------------------
     * 0 |   NO MODERATION  |   Special state : No moderation
     * 1 |     PENDING      |   Default state ; waiting for moderation
     * 2 |   IN PROGRESS    |   State when a comment is being moderated
     * 3 |    VALIDATED     |   State when a comment is successfuly moderated and visible
     * 4 |     DELETED      |   State when a comment has been moderated and censored
     * 5 |      SPAM        |   Special state used to flag spam
     * 6 | HIDDEN_BY_PARENT |   Special state when a parent comment was hidden
     */

    const STATE_NO_MODERATION = 0;

    const STATE_PENDING = 1;

    const STATE_IN_PROGRESS = 2;

    const STATE_VALIDATED = 3;

    const STATE_DELETED = 4;

    const STATE_SPAM = 5;

    const STATE_HIDDEN_BY_PARENT = 6;


    function getId();
    function getBody();
    function setBody($body);
    function getCreatedAt();
    function getThread();
    function setThread(ThreadInterface $thread);
    function getParent();
    function setParent(CommentInterface $comment);
    function getModerationState();
    function setModerationState($state);
    function getPreviousModerationState();
    static function getModerationStates();
    function getUserId();
    function setUserId($userId);
    function getUsername();
    function setUsername($username);
    function getAvatar();
    function setAvatar($avatar);
    function getEmail();
    function setEmail($email);
    function getHeaders();
    function setHeaders($headers);
    function getIp();
    function setIp();
    function getUpvotes();
    function setUpvotes($upvotes);
    function getDownvotes();
    function setDownvotes($downvotes);
    function getScore();
    function getRating();
    function setRating($rating);
    function setModerationCause($moderationCause);
    function getModerationCause();
    function setModeratedBy($moderatedBy);
    function getModeratedBy();
    function getModeratedOn();
    function setModeratedOn(\DateTime $date);
    function setCookie($cookie);
    function getCookie();
    function isVisible();
    function setVisibility($visibility);
}
