<?php

namespace La\CommentBundle\Model;

/**
 * Bans tracking Interface
 */
interface BanInterface
{
    /**
     * @param string $banCause
     */
    function setBanCause($banCause);

    /**
     * @return string
     */
    function getBanCause();

    /**
     * @param string $bannedBy
     */
    function setBannedBy($bannedBy);

    /**
     * @return string
     */
    function getBannedBy();

    /**
     * @param \DateTime $bannedSince
     */
    function setBannedSince($bannedSince);

    /**
     * @return \DateTime
     */
    function getBannedSince();

    /**
     * @param \DateTime $bannedUntil
     */
    function setBannedUntil($bannedUntil);

    /**
     * @return \DateTime
     */
    function getBannedUntil();

    /**
     * @param \La\CommentBundle\Model\CommentInterface $comment
     */
    function setComment($comment);

    /**
     * @return \La\CommentBundle\Model\CommentInterface
     */
    function getComment();

    /**
     * @param string $cookie
     */
    function setCookie($cookie);

    /**
     * @return string
     */
    function getCookie();

    /**
     * @param string $email
     */
    function setEmail($email);

    /**
     * @return string
     */
    function getEmail();

    /**
     * @param mixed $id
     */
    function setId($id);

    /**
     * @return mixed
     */
    function getId();

    /**
     * @param mixed $id
     */
    public function setUserId($id);

    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * @param string $ip
     */
    function setIp($ip);

    /**
     * @return string
     */
    function getIp();

    /**
     * @param string $username
     */
    function setUsername($username);

    /**
     * @return string
     */
    function getUsername();

}
