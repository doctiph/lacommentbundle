<?php



namespace La\CommentBundle\Model;

/*
 * Binds a comment tree to anything, using a unique, arbitrary id
 *
 */
interface ThreadInterface
{

    const THREAD_TYPE = 0;
    const REVIEW_TYPE = 1;

    /**
     * Id, a unique string that binds the comments together in a thread (tree).
     * It can be a url or really anything unique.
     *
     * @return string
     */
    public function getId();

    /**
     * @param string
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getCreated();

    /**
     * Url of the page where the thread lives
     * @return string
     */
    public function getPermalink();

    /**
     * @param  string
     * @return null
     */
    public function setPermalink($permalink);

    /**
     * Tells if new comments can be added in this thread
     *
     * @return bool
     */
    public function isCommentable();

    /**
     * @param bool $isCommentable
     */
    public function setCommentable($isCommentable);

    /**
     * Gets the number of comments
     *
     * @return integer
     */
    public function getNumComments();

    /**
     * Sets the number of comments
     *
     * @param integer $numComments
     */
    public function setNumComments($numComments);

    /**
     * Increments the number of comments by the supplied
     * value.
     *
     * @param  integer $by The number of comments to increment by
     * @return integer The new comment total
     */
    public function incrementNumComments($by);

    /**
     * Denormalized date of the last comment
     * @return \DateTime
     */
    public function getLastCommentAt();

    /**
     * @param  DateTime
     * @return null
     */
    public function setLastCommentAt($lastCommentAt);

    /**
     * @return int
     */
    public function getType();

    /**
     * @param $type
     */
    public function setType($type);

    /** ratings */
    public function getRating();
    public function addRating($rating);
    public function removeRating($rating);
    public function setRating($rating);
    public function setRatingCount($count);
    public function setRatingSum($sum);
}
