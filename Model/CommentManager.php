<?php



namespace La\CommentBundle\Model;

use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Event\CommentPersistEvent;
use La\CommentBundle\Sorting\SortingFactory;
use La\CommentBundle\Sorting\SortingInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use InvalidArgumentException;

/**
 * Abstract Comment Manager implementation which can be used as base class for your
 * concrete manager.
 *
 */
abstract class CommentManager implements CommentManagerInterface
{
    /**
     * @var SortingFactory
     */
    protected $sortingFactory;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var bool
     */
    protected $aPrioriModeration;

    /**
     * Constructor
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \La\CommentBundle\Sorting\SortingFactory $factory
     * @param $aPrioriModeration
     */
    public function __construct(EventDispatcherInterface $dispatcher, SortingFactory $factory, $aPrioriModeration)
    {
        $this->dispatcher = $dispatcher;
        $this->sortingFactory = $factory;
        $this->aPrioriModeration = $aPrioriModeration;
    }

    /**
     * Returns an empty comment instance
     *
     * @param ThreadInterface $thread
     * @param CommentInterface $parent
     * @return CommentInterface
     */
    public function createComment(ThreadInterface $thread, CommentInterface $parent = null)
    {
        $class = $this->getClass();

        /** @var CommentInterface $comment */
        $comment = new $class;

        $this->setModerationState($comment, $comment::STATE_PENDING);

        $comment->setThread($thread);

        if (null !== $parent) {
            $comment->setParent($parent);
        }

        $event = new CommentEvent($comment);
        $this->dispatcher->dispatch(Events::COMMENT_CREATE, $event);

        return $comment;
    }

    /**
     * @param CommentInterface $comment
     * @param $state
     * @return mixed|void
     */
    public function setModerationState(CommentInterface $comment, $state)
    {

        $comment->setModerationState($state);

        // Handle visibility :
        switch ($state) {
            case $comment::STATE_PENDING :
                $comment->setVisibility($this->aPrioriModeration === false); // Visible if a priori moderation is false
                break;
            case $comment::STATE_VALIDATED :
                $comment->setVisibility(true);
                break;
            case $comment::STATE_DELETED :
            case $comment::STATE_SPAM :
            case $comment::STATE_HIDDEN_BY_PARENT :
                $comment->setVisibility(false);
                break;
            default:
                break;
        }
    }

    /**
     * Returns all thread comments in a nested array
     * Will typically be used when it comes to display the comments.
     *
     * @param  ThreadInterface $thread
     * @param  string $sorter
     * @param  integer $depth
     * @param null $offset
     * @param null $limit
     * @return array
     * eg:
     * array(
     *     0 => array(
     *         'comment' => CommentInterface,
     *         'children' => array(
     *             0 => array (
     *                 'comment' => CommentInterface,
     *                 'children' => array(...)
     *             ),
     *             1 => array (
     *                 'comment' => CommentInterface,
     *                 'children' => array(...)
     *             )
     *         )
     *     ),
     *     1 => array(
     *         ...
     *     )
     */
    public function findCommentTreeByThread(ThreadInterface $thread, $sorter = null, $depth = null, $offset = null, $limit = null)
    {
        $comments = $this->findCommentsByThread($thread, $depth, $sorter, $offset, $limit);
        return $this->organiseComments($comments);
    }

    /**
     * Organises a flat array of comments into a Tree structure. For
     * organising comment branches of a Tree, certain parents which
     * have not been fetched should be passed in as an array to
     * $ignoreParents.
     *
     * @param  array $comments An array of comments to organise
     * @param \La\CommentBundle\Sorting\SortingInterface|null|string $sorter The sorter to use for sorting the tree
     * @param  array|null $ignoreParents An array of parents to ignore
     * @return array       A tree of comments
     */
    protected function organiseComments($comments, SortingInterface $sorter = null, $ignoreParents = null)
    {
        $tree = new Tree();

        foreach ($comments as $comment) {
            $path = $tree;

            $ancestors = $comment->getAncestors();
            if (is_array($ignoreParents)) {
                $ancestors = array_diff($ancestors, $ignoreParents);
            }

            foreach ($ancestors as $ancestor) {
                if ($path->has($ancestor)) { // To avoid unignored parents that should have been ignored
                    $path = $path->traverse($ancestor);
                }
            }

            $path->add($comment);
        }

        $tree = $tree->toArray();
        if (!is_null($sorter)) {
            $tree = $sorter->sort($tree);
        }

        return $tree;
    }

    /**
     * Saves a comment to the persistence backend used. Each backend
     * must implement the abstract doSaveComment method which will
     * perform the saving of the comment to the backend.
     *
     * @param  CommentInterface $comment
     * @throws \InvalidArgumentException when the comment does not have a thread.
     * @return bool
     */
    public function saveComment(CommentInterface $comment)
    {

        if (null === $comment->getThread()) {
            throw new InvalidArgumentException('The comment must have a thread');
        }

        $event = new CommentPersistEvent($comment);
        $this->dispatcher->dispatch(Events::COMMENT_PRE_PERSIST, $event);

        if ($event->isPersistenceAborted()) {
            return false;
        }

        $this->doSaveComment($comment);

        $event = new CommentEvent($comment);
        $this->dispatcher->dispatch(Events::COMMENT_POST_PERSIST, $event);

        $this->doSaveThread($comment->getThread());

        return true;
    }

    /**
     * Performs the persistence of a comment.
     *
     * @abstract
     * @param CommentInterface $comment
     */
    abstract protected function doSaveComment(CommentInterface $comment);

    abstract protected function doSaveThread(ThreadInterface $thread);
}
