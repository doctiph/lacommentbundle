<?php



namespace La\CommentBundle\Model;

/**
 * Interface to be implemented by comment managers. This adds an additional level
 * of abstraction between your application, and the actual repository.
 *
 * All changes to comments should happen through this interface.
 *
 */
interface CommentManagerInterface
{
    /**
     * Returns a flat array of comments with the specified thread.
     *
     * The sorter parameter should be left alone if you are sorting in the
     * tree methods.
     *
     * @param ThreadInterface $thread
     * @param integer $depth
     * @param string $sorterAlias
     * @param null $offset
     * @param null $limit
     * @internal param null $state
     * @return array           of CommentInterface
     */
    public function findCommentsByThread(ThreadInterface $thread, $depth = null, $sorterAlias = null, $offset = null, $limit = null);

    /**
     * @param int $offset
     * @param null $limit
     * @param null $moderationState
     * @param null $sorter
     * @return mixed
     */
    public function findLastComments($offset = 0, $limit = null, $moderationState = null, $sorter = null);


    /**
     * @param int $offset
     * @param null $limit
     * @param null $sorterAlias
     * @return mixed
     */
    public function findLastModeratedComments($offset = 0, $limit = null, $sorterAlias = null);


    /**
     * @param int $offset
     * @param null $limit
     * @param null $sorterAlias
     * @return mixed
     */
    public function findRefusedComments($offset = 0, $limit = null, $sorterAlias = null);

    /*
     * Returns all thread comments in a nested array
     * Will typically be used when it comes to display the comments.
     *
     * Will query for an additional level of depth when provided
     * so templates can determine to display a 'load more comments' link.
     *
     * @param  ThreadInterface $thread
     * @param  string          $sorter The sorter to use
     * @param  integer         $depth
     * @return array(
     *     0 => array(
     *         'comment' => CommentInterface,
     *         'children' => array(
     *             0 => array (
     *                 'comment' => CommentInterface,
     *                 'children' => array(...)
     *             ),
     *             1 => array (
     *                 'comment' => CommentInterface,
     *                 'children' => array(...)
     *             )
     *         )
     *     ),
     *     1 => array(
     *         ...
     *     )
     */
    public function findCommentTreeByThread(ThreadInterface $thread, $sorter = null, $depth = null);

    /**
     * Returns a partial comment tree based on a specific parent commentId.
     *
     * @param  integer $commentId
     * @param  string $sorter The sorter to use
     * @return array   see findCommentTreeByThread()
     */
    public function findCommentTreeByCommentId($commentId, $sorter = null);

    /**
     * Saves a comment.
     *
     * @param CommentInterface $comment
     * @return
     */
    public function saveComment(CommentInterface $comment);

    /**
     * Find one comment by its ID.
     *
     * @param $id
     * @return Comment or null
     */
    public function findCommentById($id);

    /**
     * Creates an empty comment instance.
     *
     * @param ThreadInterface $thread
     * @param CommentInterface $comment
     * @return Comment
     */
    public function createComment(ThreadInterface $thread, CommentInterface $comment = null);

    /**
     * Set moderation state and update visibility if needed
     *
     * @param CommentInterface $comment
     * @param $state
     * @return mixed
     */
    public function setModerationState(CommentInterface $comment, $state);

    /**
     * Update offspring moderation state and update visibility if needed
     *
     * @param array $ancestors
     * @param $moderationState
     * @return mixed
     */
    public function updateOffspringModerationState(array $ancestors, $moderationState);

    /**
     * Checks if the comment was already persisted before, or if it's a new one.
     *
     * @param CommentInterface $comment
     *
     * @return boolean True, if it's a new comment
     */
    public function isNewComment(CommentInterface $comment);

    /**
     * Returns the comment fully qualified class name.
     *
     * @return string
     */
    public function getClass();
}
