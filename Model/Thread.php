<?php



namespace La\CommentBundle\Model;

use DateTime;

/**
 * Storage agnostic comment thread object
 *
 */
abstract class Thread implements ThreadInterface
{

    /**
     * Id, a unique string that binds the comments together in a thread (tree).
     * It can be a url or really anything unique.
     *
     * @var string
     */
    protected $id;

    /**
     * @var DateTime
     */
    protected $created;

    /**
     * Tells if new comments can be added in this thread
     *
     * @var bool
     */
    protected $isCommentable = true;

    /**
     * Tells if thread is enabled, and if it can be displayed
     */
    protected $isEnabled = true;

    /**
     * Denormalized number of comments
     *
     * @var integer
     */
    protected $numComments = 0;

    /**
     * Denormalized date of the last comment
     *
     * @var DateTime
     */
    protected $lastCommentAt = null;

    /**
     * Url of the page where the thread lives
     *
     * @var string
     */
    protected $permalink;

    /**
     * Thread type : cf. constants
     *
     * @var int
     */
    protected $type = self::THREAD_TYPE;

    /**
     * Average rating of the thread-related content, given by comments posters.
     *
     * @var int
     */
    protected $rating = 0;

    /**
     * Numbers of ratings registered for the thread
     *
     * @var int
     */
    protected $ratingCount = 0;

    /**
     * Sum of all ratings registered for the thread
     *
     * @var int
     */

    protected $ratingSum = 0;

    public function __construct(){
        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string
     * @return null
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @param  string
     * @return null
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;
    }

    /**
     * @return bool
     */
    public function isCommentable()
    {
        return $this->isCommentable;
    }

    /**
     * @param  bool
     * @return null
     */
    public function setCommentable($isCommentable)
    {
        $this->isCommentable = (bool) $isCommentable;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param  bool
     * @return null
     */
    public function setEnabled($isEnabled)
    {
        $this->isEnabled = (bool) $isEnabled;
    }

    /**
     * Gets the number of comments
     *
     * @return integer
     */
    public function getNumComments()
    {
        return $this->numComments;
    }

    /**
     * Sets the number of comments
     *
     * @param integer $numComments
     */
    public function setNumComments($numComments)
    {
        $this->numComments = intval($numComments);
    }

    /**
     * Increments the number of comments by the supplied
     * value.
     *
     * @param  integer $by Value to increment comments by
     * @return integer The new comment total
     */
    public function incrementNumComments($by = 1)
    {
        return $this->numComments += intval($by);
    }

    /**
     * @return DateTime
     */
    public function getLastCommentAt()
    {
        return $this->lastCommentAt;
    }

    /**
     * @param  DateTime
     * @return null
     */
    public function setLastCommentAt($lastCommentAt)
    {
        $this->lastCommentAt = $lastCommentAt;
    }

    /**
     * @return int
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type){
        $this->type = $type;
    }


    public function getRating()
    {
        return $this->rating;
    }

    public function addRating($rating)
    {
        $rating = intval($rating);
        if($rating > 5||$rating < 0)
        {
            throw new \Exception('Invalid note for thread '.$this->getId());
        }

        $this->ratingCount++;
        $this->ratingSum += $rating;
        $this->rating = round(($this->ratingSum/$this->ratingCount),0);
    }

    public function removeRating($rating)
    {
        $this->ratingCount--;
        $this->ratingSum -= $rating;
        $this->rating = round(($this->ratingSum/$this->ratingCount),0);
    }

    public function setRating($rating){
        $this->rating = $rating;
    }

    public function setRatingCount($count){
        $this->ratingCount = $count;
    }

    public function setRatingSum($sum){
        $this->ratingSum = $sum;
    }

}
