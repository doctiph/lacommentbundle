<?php



namespace La\CommentBundle\Model;

use La\CommentBundle\Events;
use La\CommentBundle\Event\VoteEvent;
use La\CommentBundle\Event\VotePersistEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract VotingManager
 *
 */
abstract class VoteManager implements VoteManagerInterface
{
    /**
     * @var
     */
    protected $dispatcher;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Finds a vote by id.
     *
     * @param  $id
     * @return VoteInterface
     */
    public function findVoteById($id)
    {
        return $this->findVoteBy(array('id' => $id));
    }

    /**
     * Creates a Vote object.
     *
     * @param  CommentInterface $comment
     * @return VoteInterface
     */
    public function createVote(CommentInterface $comment)
    {
        $class = $this->getClass();
        $vote = new $class();
        $vote->setComment($comment);

        $event = new VoteEvent($vote);
        $this->dispatcher->dispatch(Events::VOTE_CREATE, $event);

        return $vote;
    }

    public function saveVote(VoteInterface $vote)
    {
        if (null === $vote->getComment()) {
            throw new \InvalidArgumentException('Vote passed into saveVote must have a comment');
        }

        $event = new VotePersistEvent($vote);
        $this->dispatcher->dispatch(Events::VOTE_PRE_PERSIST, $event);

        if ($event->isPersistenceAborted()) {
            return;
        }

        $this->doSaveVote($event->getVote());

        $event = new VoteEvent($vote);
        $this->dispatcher->dispatch(Events::VOTE_POST_PERSIST, $event);
    }

    /**
     * Performs the persistence of the Vote.
     *
     * @abstract
     * @param VoteInterface $vote
     */
    abstract protected function doSaveVote(VoteInterface $vote);
}
