<?php



namespace La\CommentBundle\Model;

use Symfony\Component\Validator\ExecutionContext;

/**
 * Methods a vote should implement.
 *
 */
interface VoteInterface
{
    const VOTE_UP = 1;
    const VOTE_DOWN = -1;

    /**
     * @return mixed unique ID for this vote
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getComment();

    /**
     * @param CommentInterface $comment
     * @return mixed
     */
    public function setComment(CommentInterface $comment);

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param $value
     * @return mixed
     */
    public function setValue($value);

    /**
     * @return mixed user Id
     */
    public function getUser();

    /**
     * @param $user
     * @return mixed
     */
    public function setUser($user);

    /**
     * @param ExecutionContext $context
     */
    public function isVoteValid(ExecutionContext $context);
}
