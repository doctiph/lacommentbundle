<?php



namespace La\CommentBundle\Model;

use DateTime;
use InvalidArgumentException;

/**
 * Storage agnostic comment object
 *
 */
abstract class Comment implements CommentInterface
{
    /**
     * Comment id
     *
     * @var mixed
     */
    protected $id;

    /**
     * Parent comment id
     *
     * @var CommentInterface
     */
    protected $parent;

    /**
     * Comment text
     *
     * @var string
     */
    protected $body;

    /**
     * The depth of the comment
     *
     * @var integer
     */
    protected $depth = 0;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * Current moderation state of the comment.
     *
     * @var integer
     */
    protected $moderationState = 1;

    /**
     * The previous moderation state of the comment.
     *
     * @var integer
     */
    protected $previousModerationState = 1;

    /**
     * Current visibility of the comment
     *
     * @var boolean
     */
    protected $visibility = true;

    /**
     * Comment title
     *
     * @var string
     */
    protected $title;

    /**     *
     * @var ThreadInterface
     */
    protected $thread;

    /**
     * user id of comment author
     *
     * @var string
     */
    protected $userId;

    /**
     * username of comment author
     *
     * @var string
     */
    protected $username;

    /**
     * avatar of comment author
     *
     * @var string
     */
    protected $avatar;

    /**
     * email of comment author
     *
     * @var email
     */
    protected $email;

    /**
     * Headers from the HTTP Request sent when commenting
     *
     * @var string
     */
    protected $headers = '';

    /**
     * IP based on headers
     *
     * @var string
     */
    protected $ip = '';

    /**
     * Upvotes from users reading the comment
     *
     * @var int
     */
    protected $upvotes = 0;

    /**
     * Downvotes from users reading the comment
     *
     * @var int
     */
    protected $downvotes = 0;

    /**
     * $total upvotes - downvotes
     *
     * @var int
     */
    protected $score;

    /**
     * Rating of the content commented, given by comment author
     *
     * @var int
     */
    protected $rating = 0;

    /**
     * Unique cookie identifying a user
     *
     * @var string
     */
    protected $cookie;

    /**
     * If the comment has been moderated, code of the cause of moderation. Refer to moderation bundle to get literal cause.
     *
     * @var int
     */
    protected $moderationCause = 0;

    /**
     * If the comment has been moderated, name of the moderator.
     *
     * @var string
     */
    protected $moderatedBy;

    /**
     * Date on which the comment has been moderated
     *
     * @var \DateTime
     */
    protected $moderatedOn = null;

    public function __construct()
    {
        $this->score = $this->getScore();
        $this->createdAt = new DateTime();
    }

    /**
     * Return the comment unique id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $cookie
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param  string
     * @return null
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @param $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets the creation date
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Returns the depth of the comment.
     *
     * @return integer
     */
    public function getDepth()
    {
        return $this->depth;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(CommentInterface $parent)
    {
        $this->parent = $parent;

        if (!$parent->getId()) {
            throw new InvalidArgumentException('Parent comment must be persisted.');
        }

        $ancestors = $parent->getAncestors();
        $ancestors[] = $parent->getId();

        $this->setAncestors($ancestors);
    }

    /**
     * @return ThreadInterface
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param ThreadInterface $thread
     *
     * @return void
     */
    public function setThread(ThreadInterface $thread)
    {
        $this->thread = $thread;
    }

    /**
     * {@inheritDoc}
     */
    public static function getModerationStates()
    {
        $class = new \ReflectionClass(__CLASS__);
        return $class->getConstants();
    }

    /**
     * {@inheritDoc}
     */
    public function getModerationState()
    {
        return $this->moderationState;
    }

    /**
     * {@inheritDoc}
     */
    public function setModerationState($state)
    {
        $this->previousModerationState = $this->moderationState;
        $this->moderationState = $state;
    }

    /**
     * {@inheritDoc}
     */
    public function getPreviousModerationState()
    {
        return $this->previousModerationState;
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->visibility;
    }

    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        $this->setIp();
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * return $ip
     */
    public function setIp()
    {
        if ($this->headers != '') {
            $headers = unserialize($this->headers);

            if (isset($headers['HTTP_TRUE_CLIENT_IP'])) {
                $this->ip = $headers['HTTP_TRUE_CLIENT_IP'];
            } elseif (isset($headers['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $headers['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($headers['REMOTE_ADDR'])) {
                $this->ip = $headers['REMOTE_ADDR'];
            }
        }
        return $this->ip;
    }

    /**
     * @return int
     */
    public function getUpvotes()
    {
        return $this->upvotes;
    }

    /**
     * @param int $upvotes
     */
    public function setUpvotes($upvotes)
    {
        $this->upvotes = $upvotes;
        $this->score = $this->getScore();
    }

    /**
     * @return int
     */
    public function getDownvotes()
    {
        return $this->downvotes;
    }

    /**
     * @param int $downvotes
     */
    public function setDownvotes($downvotes)
    {
        $this->downvotes = $downvotes;
        $this->score = $this->getScore();
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return ($this->upvotes - $this->downvotes);
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @param $moderationCause
     */
    public function setModerationCause($moderationCause)
    {
        $this->moderationCause = $moderationCause;
    }

    /**
     * @return int
     */
    public function getModerationCause()
    {
        return $this->moderationCause;
    }

    /**
     * @param string $moderatedBy
     */
    public function setModeratedBy($moderatedBy)
    {
        $this->moderatedBy = $moderatedBy;
    }

    /**
     * @return string
     */
    public function getModeratedBy()
    {
        return $this->moderatedBy;
    }

    /**
     * @param \DateTime $moderatedOn
     */
    public function setModeratedOn(\DateTime $moderatedOn)
    {
        $this->moderatedOn = $moderatedOn;
    }

    /**
     * @return \DateTime
     * @return \DateTime|null
     */
    public function getModeratedOn()
    {
        return $this->moderatedOn;
    }

}
