<?php

namespace La\CommentBundle\Model;


/**
 * Bans tracking
 */
abstract class Ban implements BanInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var int;
     */
    protected $userId;

    /**
     * Username of the banned user
     *
     * @var string
     */
    protected $username;

    /**
     * Email of the banned user
     *
     * @var string
     */
    protected $email;

    /**
     * Ip of the banned user
     *
     * @var string
     */
    protected $ip;

    /**
     * Cookie of the banned user
     *
     * @var string
     */
    protected $cookie;

    /**
     * Beginning of the ban
     *
     * @var \DateTime
     */
    protected $bannedSince;

    /**
     * End of the ban
     *
     * @var \DateTime
     */
    protected $bannedUntil;

    /**
     * Comment id that led to the ban
     *
     * @var CommentInterface
     */
    protected $comment;

    /**
     * Cause of the ban
     *
     * @var string
     */
    protected $banCause;

    /**
     * Author of the ban
     *
     * @var string
     */
    protected $bannedBy;

    /**
     * @param string $banCause
     */
    public function setBanCause($banCause)
    {
        $this->banCause = $banCause;
    }

    /**
     * @return string
     */
    public function getBanCause()
    {
        return $this->banCause;
    }

    /**
     * @param string $bannedBy
     */
    public function setBannedBy($bannedBy)
    {
        $this->bannedBy = $bannedBy;
    }

    /**
     * @return string
     */
    public function getBannedBy()
    {
        return $this->bannedBy;
    }

    /**
     * @param \DateTime $bannedSince
     */
    public function setBannedSince($bannedSince)
    {
        $this->bannedSince = $bannedSince;
    }

    /**
     * @return \DateTime
     */
    public function getBannedSince()
    {
        return $this->bannedSince;
    }

    /**
     * @param \DateTime $bannedUntil
     */
    public function setBannedUntil($bannedUntil)
    {
        $this->bannedUntil = $bannedUntil;
    }

    /**
     * @return \DateTime
     */
    public function getBannedUntil()
    {
        return $this->bannedUntil;
    }

    /**
     * @param \La\CommentBundle\Model\CommentInterface $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \La\CommentBundle\Model\CommentInterface
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $cookie
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setUserId($id)
    {
        $this->userId = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }



}
