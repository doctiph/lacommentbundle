<?php



namespace La\CommentBundle\Model;

use DateTime;

use Symfony\Component\Validator\ExecutionContext;

/**
 * Storage agnostic vote object - Requires La\UserBundle
 *
 */
abstract class Vote implements VoteInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var CommentInterface
     */
    protected $comment;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * The value of the vote.
     *
     * @var integer
     */
    protected $value;

    public function __construct(CommentInterface $comment = null)
    {
        $this->comment = $comment;
    }

    /**
     * Return the comment unique id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer The votes value.
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param integer $value
     */
    public function setValue($value)
    {
        $this->value = intval($value);
    }

    /**
     * {@inheritdoc}
     */
    public function isVoteValid(ExecutionContext $context)
    {
        if (!$this->checkValue($this->value)) {
            return false;
        }
        return true;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Checks if the value is an appropriate one.
     *
     * @param mixed $value
     *
     * @return boolean True, if the integer representation of the value is not null or 0.
     */
    protected function checkValue($value)
    {
        return null !== $value && intval($value);
    }

    /**
     * @param CommentInterface $comment
     */
    public function setComment(CommentInterface $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \La\CommentBundle\Model\CommentInterface
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


}
