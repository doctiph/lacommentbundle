<?php
namespace La\CommentBundle\Command;

use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\DialogHelper;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use La\CommentBundle\Model\ThreadInterface;
use La\CommentBundle\Entity\CommentManager;
use La\CommentBundle\Entity\ThreadManager;


/**
 *
 * php app/console comments:update:count {--thread = ? | --all }

 */
class UpdateCommentsCountCommand extends ContainerAwareCommand
{
    /** @var  ThreadManager */
    protected $threadManager;
    /** @var  CommentManager */
    protected $commentManager;

    /** @var  OutputInterface */
    protected $output;
    /** @var  DialogHelper */
    protected $dialog;
    /** @var  ProgressHelper */
    protected $progress;
    /** @var  Logger */
    protected $logger;

    protected function configure()
    {
        $this
            ->setName('comments:update:count')
            ->setDescription('Update comments count for a given thread')
            ->addOption(
                'thread',
                't',
                InputOption::VALUE_OPTIONAL,
                'thread id',
                null)
            ->addOption(
                'all',
                null,
                InputOption::VALUE_NONE,
                'update all threads');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $this->output = $output;
        $this->output->getFormatter()->setStyle('fire', new OutputFormatterStyle('red', 'yellow', array('bold', 'blink')));

        $this->dialog = $this->getHelperSet()->get('dialog');
        $this->progress = $this->getHelperSet()->get('progress');
        $this->logger = $this->getContainer()->get('logger');

        $this->commentManager = $this->getContainer()->get('la_comment.manager.comment');
        $this->threadManager = $this->getContainer()->get('la_comment.manager.thread');

        $thread = $input->getOption('thread');
        $all = $input->getOption('all');

        try {

            if (!is_null($thread)) {
                $thread = $this->threadManager->findThreadById($thread);
                if (is_null($thread)) {
                    throw new \Exception("Invalid thread id.");
                }
                $this->updateThreadCommentCount($thread, true);
            } else if ($all) {

                $this->output->writeln("Updating each thread. This may take a while.");
                $this->threadManager->disableLogger();
                $count = $this->threadManager->findThreadsCount();

                if (!$this->dialog->askConfirmation(
                    $this->output,
                    sprintf('<fire>%d threads found. Coninue update ? (y/n)</fire> : ', $count),
                    false
                )
                ) {
                    $this->output->writeln("Aborted.") . PHP_EOL;
                    return;
                }


                $this->progress->start($this->output, $count);

                $threads = $this->threadManager->findAllThreadsIterable();
                foreach ($threads as $thread) {
                    $this->updateThreadCommentCount($thread[0]);
                    $this->progress->advance();
                    echo sprintf(" -- memory usage : %d Mo", memory_get_usage(true) / (1024 * 1024));
                    unset($thread);
                }
                unset($threads);

                $this->progress->finish();
                $this->output->writeln(PHP_EOL . "Threads were successfuly updated.");

            } else {
                throw new \Exception("You must specify a thread or use --all.");
            }


        } catch
        (\Exception $e) {
            $this->output->writeln($e->getMessage());
            $this->logger->log($this->logger->getLevels()['WARNING'], $e->getMessage());
        }

    }

    protected function updateThreadCommentCount(ThreadInterface $thread, $output = FALSE)
    {
        $oldCount = $thread->getNumComments();

        // UPDATE THREAD COMMENT COUNT :
        $count = $this->commentManager->countVisibleCommentsForThread($thread);

        if ($oldCount != $count) {
            $thread->setNumComments($count);
            // UPDATE THREAD RATING COUNT
            if ($count) {
                $thread->setLastCommentAt($this->commentManager->findLastCommentDate($thread));
            } else{
                $thread->setLastCommentAt(null);
            }
            $string = sprintf("Thread #%s updated (%d was changed to %d).", $thread->getId(), $oldCount, $count);
            if ($thread->getType() == $thread::REVIEW_TYPE) {
                $this->commentManager->updateThreadRating($thread);
                $string .= " Rating was updated.";
            }
            $this->threadManager->saveThread($thread);

        } else {
            $string = sprintf("Thread #%s already up to date.", $thread->getId());
        }
        $this->threadManager->clear($thread);

        if ($output) {
            $this->logger->log($this->logger->getlevels()['INFO'], sprintf("%s (Memory : %d Mo)", $string, memory_get_usage(true) / (1024 * 1024)));
        }
    }
}
