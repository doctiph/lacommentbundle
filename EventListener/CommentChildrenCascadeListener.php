<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Model\CommentInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Model\CommentManagerInterface;

/**
 * A listener that updates children moderation state on parent state change
 */
class CommentChildrenCascadeListener implements EventSubscriberInterface
{
    /**
     * @var CommentManagerInterface
     */
    protected $commentManager;

    /**
     * @var bool
     */
    protected $moderationCascade;

    /**
     * Constructor.
     *
     * @param CommentManagerInterface $commentManager
     * @param $moderationCascade
     */
    public function __construct(CommentManagerInterface $commentManager, $moderationCascade)
    {
        $this->commentManager = $commentManager;
        $this->moderationCascade = $moderationCascade;
    }

    /**
     * Increase the thread comments number
     *
     * @param \La\CommentBundle\Event\CommentEvent $event
     */
    public function onCommentPersist(CommentEvent $event)
    {
        /** @var CommentInterface $comment */
        $comment = $event->getComment();
        $id = $comment->getId();


        if ($this->moderationCascade && !is_null($id)) {

            if ($comment->getModerationState() != $comment->getPreviousModerationState() && !$comment->isVisible()) {
                $childrenAncestors = array_merge($comment->getAncestors(), array($comment->getId()));
                $this->commentManager->updateOffspringModerationState($childrenAncestors, $comment::STATE_HIDDEN_BY_PARENT);
            }

        }
    }

    public static function getSubscribedEvents()
    {
        return array(Events::COMMENT_PRE_PERSIST => 'onCommentPersist');
    }
}
