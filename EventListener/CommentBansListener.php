<?php



namespace La\CommentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentPersistEvent;
use La\CommentBundle\Model\BanManagerInterface;
use La\CommentBundle\Model\BanInterface;

/**
 * A listener that checks if a comment is spam based on a service
 * that implements SpamDetectionInterface.
 *
 */
class CommentBansListener implements EventSubscriberInterface
{
    /**
     * @var BanManagerInterface
     */
    protected $banManager;

    /**
     * @param BanManagerInterface $banManager
     */
    public function __construct(BanManagerInterface $banManager)
    {
        $this->banManager = $banManager;
    }

    public function rejectIfBanned(CommentPersistEvent $event)
    {
        $comment = $event->getComment();

        $commentUserData = array(
            'userId' => $comment->getUserId(),
            'username' => $comment->getUsername(),
            'email' => $comment->getEmail(),
            'ip' => $comment->getIp(),
            'cookie' => $comment->getCookie(),
        );

        foreach($commentUserData as $key => $data){
            /** @var BanInterface $ban */
            $ban = $this->banManager->findBanBy( array($key => $data) );

            if(!is_null($ban) && $ban->getBannedUntil()->diff(new \DateTime()) == 0){   // diff == 0 means ban hasnt expired, abort persistence.
                $event->abortPersistence();
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(Events::COMMENT_PRE_PERSIST => 'rejectIfBanned');
    }
}
