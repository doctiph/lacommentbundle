<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\ThreadInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Entity\CommentManager;

/**
 * A listener that updates thread counters when a new comment is made.
 */
class ThreadCountersListener implements EventSubscriberInterface
{
    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * Constructor.
     *
     * @param CommentManager $commentManager
     */
    public function __construct(CommentManager $commentManager)
    {
        $this->commentManager = $commentManager;
    }

    /**
     * Increase the thread comments number
     *
     * @param \La\CommentBundle\Event\CommentEvent $event
     */
    public function onCommentPersist(CommentEvent $event)
    {
        /** @var CommentInterface $comment */
        $comment = $event->getComment();
        /** @var ThreadInterface $thread */
        $thread = $comment->getThread();

        if ($this->commentManager->isNewComment($comment)) {
            $thread->setLastCommentAt($comment->getCreatedAt());
        }
    }

    public function onCommentPersisted(CommentEvent $event)
    {
        /** @var CommentInterface $comment */
        $comment = $event->getComment();
        /** @var ThreadInterface $thread */
        $thread = $comment->getThread();

        // UPDATE THREAD COMMENT COUNT :
        $count = $this->commentManager->countVisibleCommentsForThread($thread);
        $thread->setNumComments($count);
        if ($count) {
            $thread->setLastCommentAt($this->commentManager->findLastCommentDate($thread));
        }
        if ($thread->getType() == $thread::REVIEW_TYPE) {
            // UPDATE THREAD RATING COUNT
            $this->commentManager->updateThreadRating($thread);
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            Events::COMMENT_PRE_PERSIST => array('onCommentPersist', 0),
            Events::COMMENT_POST_PERSIST => array('onCommentPersisted', 0)
        );
    }
}
