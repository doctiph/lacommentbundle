<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentPersistEvent;
use La\CommentBundle\SpamDetection\SpamDetectionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * A listener that checks if a comment is spam based on a service
 * that implements SpamDetectionInterface.
 *
 */
class CommentSpamListener implements EventSubscriberInterface
{
    /**
     * @var SpamDetectionInterface
     */
    protected $spamDetector;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor.
     *
     * @param SpamDetectionInterface $detector
     * @param LoggerInterface        $logger
     */
    public function __construct(SpamDetectionInterface $detector, LoggerInterface $logger = null)
    {
        $this->spamDetector = $detector;
        $this->logger = $logger;
    }

    public function spamCheck(CommentPersistEvent $event)
    {
        $comment = $event->getComment();

        if ($this->spamDetector->isSpam($comment)) {
            if (null !== $this->logger) {
                $this->logger->info('Comment is marked as spam from detector, aborting persistence.');
            }

            $event->abortPersistence();
        }
    }

    public static function getSubscribedEvents()
    {
        return array(Events::COMMENT_PRE_PERSIST => 'spamCheck');
    }
}
