<?php

namespace La\CommentBundle\EventListener;

use La\ApiBundle\EventListener\ApiDocListener as BaseApiDocListener;


class ApiDocListener extends BaseApiDocListener
{
    const API_DOC_ROOT_EVENT_PRIORITY = 60;

    public function onApiDocRootRequest($event)
    {
        $event->addDocRootDir(__DIR__ . '/../Resources/doc/api/resources');
    }


}
