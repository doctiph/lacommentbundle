<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Markup\ParserInterface;
use La\CommentBundle\Model\RawCommentInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Parses a comment for markup and sets the result
 * into the rawBody property.
 *
 */
class CommentMarkupListener implements EventSubscriberInterface
{
    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     * Constructor.
     *
     * @param \La\CommentBundle\Markup\ParserInterface $parser
     */
    public function __construct(ParserInterface $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Parses raw comment data and assigns it to the rawBody
     * property.
     *
     * @param \La\CommentBundle\Event\CommentEvent $event
     */
    public function markup(CommentEvent $event)
    {
        $comment = $event->getComment();

        if (!$comment instanceof RawCommentInterface) {
            return;
        }

        $result = $this->parser->parse($comment->getBody());
        $comment->setRawBody($result);
    }

    public static function getSubscribedEvents()
    {
        return array(Events::COMMENT_PRE_PERSIST => 'markup');
    }
}
