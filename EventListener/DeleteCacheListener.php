<?php

namespace La\CommentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\CommentBundle\Cache\CacheManagerInterface;
use La\CommentBundle\Entity\CommentManager;
use La\CommentBundle\Entity\Comment;
use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Event\ThreadEvent;

class DeleteCacheListener implements EventSubscriberInterface
{
    protected $cacheManager;
    protected $commentManager;

    public function __construct(CacheManagerInterface $cacheManager, CommentManager $commentManager)
    {
        $this->cacheManager = $cacheManager;
        $this->commentManager = $commentManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            Events::COMMENT_PRE_PERSIST => array('onCommentPersist', -1),
            Events::THREAD_PRE_PERSIST => array('onThreadPersist', -1)
        );
    }

    public function onCommentPersist(CommentEvent $event)
    {
        /** @var Comment $comment */
        $comment = $event->getComment();
        $this->cacheManager->deleteByTags($this->cacheManager->getArrayKeysForThreadCache($comment->getThread()->getId()));
    }

    public function onThreadPersist(ThreadEvent $event)
    {
        $thread = $event->getThread();
        $this->cacheManager->deleteByTags($this->cacheManager->getArrayKeysForThreadCache($thread->getId()));
    }


}