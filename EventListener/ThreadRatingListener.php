<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Events;
use La\CommentBundle\Event\CommentEvent;
use La\CommentBundle\Model\Comment;
use La\CommentBundle\Model\CommentManagerInterface;
use La\CommentBundle\Model\Thread;
use La\CommentBundle\Model\ThreadManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A listener that updates thread rating when a new comment is made.
 *
 */
class ThreadRatingListener implements EventSubscriberInterface
{
    /**
     * @var CommentManagerInterface
     */
    private $commentManager;

    /**
     * Constructor.
     *
     * @param CommentManagerInterface $commentManager
     * @param \La\CommentBundle\Model\ThreadManagerInterface $threadManager
     */
    public function __construct(CommentManagerInterface $commentManager, ThreadManagerInterface $threadManager)
    {
        $this->commentManager = $commentManager;
        $this->threadManager = $threadManager;
    }

    /**
     * Increase the thread comments number
     *
     * @param \La\CommentBundle\Event\CommentEvent $event
     */
    public function onCommentPersist(CommentEvent $event)
    {
        /** @var Comment $comment */
        $comment = $event->getComment();
        /** @var Thread $thread */
        $thread = $comment->getThread();

        if ($thread->getType() == $thread::REVIEW_TYPE) {

            $parent = $comment->getParent();
            // Rating should be registered if (and only if) the comment has no ancestor.

            if (!is_null($comment->getRating()) && is_null($parent)) {

                if ($this->commentManager->isNewComment($comment)) {
                    if ($comment->isVisible()) {
                        $thread->addRating($comment->getRating());
                    }
                } else {
                    if ($comment->hasBecomeInvisible()) {
                        $thread->removeRating($comment->getRating());
                    } elseif ($comment->hasBecomeVisible()) {
                        $thread->addRating($comment->getRating());
                    }
                }
                $this->threadManager->saveThread($thread);

            } else if(!is_numeric($comment->getState())){
                $comment->setRating(0);
            }
            $this->threadManager->saveThread($thread);
        }

    }

    public static function getSubscribedEvents()
    {
        return array(Events::COMMENT_PRE_PERSIST => 'onCommentPersist');
    }
}