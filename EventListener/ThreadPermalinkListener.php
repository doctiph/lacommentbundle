<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Events;
use La\CommentBundle\Event\ThreadEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Responsible for setting a permalink for each new Thread object.
 *
 */
class ThreadPermalinkListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Creates and persists a thread with the specified id.
     *
     * @param \La\CommentBundle\Event\ThreadEvent $event
     */
    public function onThreadCreate(ThreadEvent $event)
    {
        if (!$this->container->isScopeActive('request')) {
            return;
        }

        $thread = $event->getThread();
        $thread->setPermalink($this->container->get('request')->getUri());
    }

    public static function getSubscribedEvents()
    {
        return array(Events::THREAD_CREATE => 'onThreadCreate');
    }
}
