<?php

namespace La\CommentBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\AdminBundle\Event\AdminNavEvent;
use La\AdminBundle\Controller\AdminController;

class AdminListener implements EventSubscriberInterface
{
    const ADMIN_NAV_EVENT_PRIORITY = 1;

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'admin.nav.links' => array('onAdminNavLinksRequest', static::ADMIN_NAV_EVENT_PRIORITY)
        );
    }

    public function onAdminNavLinksRequest(AdminNavEvent $event)
    {
        $event->addLinks(
            array(
                'name' => 'la_comment_admin.nav.stats.default',
                'icon' => 'bar-chart-o',
                'links' => array(
                    array(
                        'name' => 'la_comment_admin.nav.stats.comments_monthly',
                        'url' => $this->router->generate('la_comment_admin_stats_monthly'),
                        'role' => 'ROLE_LA_ADMIN',
                    ),
                    array(
                        'name' => 'la_comment_admin.nav.stats.comments_yearly',
                        'url' => $this->router->generate('la_comment_admin_stats_yearly'),
                        'role' => 'ROLE_LA_ADMIN',
                    ),
                )
            )
        );

        $event->addLinks(
            array(
                'name' => 'la_comment_admin.nav.comments.default',
                'icon' => 'comment',
                'links' => array(
                    array(
                        'name' => 'la_comment_admin.nav.comments.moderate',
                        'url' => $this->router->generate('la_comment_moderation_pending'),
                        'role' => 'ROLE_LA_ADMIN_LA_COMMENTS_MODERATE',
                    ),
                    array(
                        'name' => 'la_comment_admin.nav.comments.recents',
                        'url' => $this->router->generate('la_comment_moderation_recents'),
                        'role' => 'ROLE_LA_ADMIN_LA_COMMENTS_MODERATE',
                    ),
                    array(
                        'name' => 'la_comment_admin.nav.comments.refused',
                        'url' => $this->router->generate('la_comment_moderation_refused'),
                        'role' => 'ROLE_LA_ADMIN_LA_COMMENTS_MODERATE',
                    ),
                    array(
                        'name' => 'la_comment_admin.nav.comments.alerts',
                        'url' => $this->router->generate('la_comment_moderation_alerts'),
                        'role' => 'ROLE_LA_ADMIN_LA_COMMENTS_MODERATE',
                    ),
                    array(
                        'name' => 'la_comment_admin.nav.comments.search',
                        'url' => $this->router->generate('la_comment_moderation_search'),
                        'role' => 'ROLE_LA_ADMIN_LA_COMMENTS_MODERATE',
                    )
                )
            )
        );
    }

}