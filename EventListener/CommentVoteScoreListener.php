<?php



namespace La\CommentBundle\EventListener;

use La\CommentBundle\Events;
use La\CommentBundle\Event\VoteEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A listener that increments the comments vote score when a
 * new vote has been added.
 *
 */
class CommentVoteScoreListener implements EventSubscriberInterface
{
    public function onVotePersist(VoteEvent $event)
    {
        $vote = $event->getVote();
        $comment = $vote->getComment();

        switch($vote->getValue()){
            case 1:
                $comment->setUpvotes($comment->getUpvotes() + 1);
                break;
            case -1:
                $comment->setDownvotes($comment->getDownvotes() + 1);
                break;
            default:
                throw new \InvalidArgumentException('Invalid vote value.');
        }
    }

    public static function getSubscribedEvents()
    {
        return array(Events::VOTE_PRE_PERSIST => array('onVotePersist', 10));
    }
}
