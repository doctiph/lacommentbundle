<?php

namespace La\CommentBundle\EventListener;

use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\VoteManagerInterface;
use La\CommentBundle\Events;
use La\CommentBundle\Event\VotePersistEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A listener that increments the comments vote score when a
 * new vote has been added.
 *
 */
class CommentVoteUpdateListener implements EventSubscriberInterface
{
    protected $voteManager;

    public function __construct(VoteManagerInterface $voteManager)
    {
        $this->voteManager = $voteManager;
    }

    public function onVotePersist(VotePersistEvent $event)
    {
        $vote = $event->getVote();

        $existing_vote = $this->voteManager->findVoteByCommentAndAuthor($vote->getComment(), $vote->getUser());

        if (!is_null($existing_vote) ) {

            if($existing_vote->getValue() === $vote->getValue()){
                $event->abortPersistence();
                $event->stopPropagation();
            }else {
                $existing_vote->setComment($vote->getComment());
                $this->eraseLastValue($existing_vote->getValue(), $existing_vote->getComment());
                $existing_vote->setValue($vote->getValue());

                $event->setVote($existing_vote);
            }
        }

    }

    public static function getSubscribedEvents()
    {
        return array(Events::VOTE_PRE_PERSIST => array('onVotePersist', 1000));
    }

    protected function eraseLastValue($lastValue, CommentInterface $comment)
    {
        switch($lastValue){
            case 1:
                $comment->setUpvotes($comment->getUpvotes() - 1);
                break;
            case -1:
                $comment->setDownvotes($comment->getDownvotes() - 1);
                break;
            default:
                break;
        }
    }
}
