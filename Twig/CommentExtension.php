<?php

namespace La\CommentBundle\Twig;

use La\CommentBundle\Model\CommentInterface;
use La\CommentBundle\Model\ModerationManager;

class CommentExtension extends \Twig_Extension
{
/** @var ModerationManagerInterface  */
    var $moderationManager;


    public function __construct(ModerationManager $moderationManager){
        $this->moderationManager = $moderationManager;
    }

    public function getFilters()
    {
        return array(
            'moderationVerdict' => new \Twig_Filter_Method($this, 'moderationVerdictFilter'),
            'isValidated' => new \Twig_Filter_Method($this, 'isValidatedFilter'),
        );
    }

    /**
     * @param CommentInterface $comment
     * @return string
     */
    public function moderationVerdictFilter(CommentInterface $comment)
    {
        if($comment->getModerationState() === $comment::STATE_VALIDATED)
        {
            $verdict = "Validé";
        }else{
            $verdict = "Refusé";
            if($comment->getModerationCause() != null && $comment->getModerationCause() != ""){
                $verdict .= " : ".$this->moderationManager->getModerationCauses()[$comment->getModerationCause()];
            }
        }
        return $verdict;
    }

    /**
     * @param CommentInterface $comment
     * @return bool
     */
    public function isValidatedFilter(CommentInterface $comment)
    {
        if($comment->getModerationState() === $comment::STATE_VALIDATED){
            return true;
        }
        return false;
    }

    public function getName(){
        return "comments_twig_extension";
    }


}
