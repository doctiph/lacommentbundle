var la_comment_alert;

la_comment_alert = (function () {
    "use strict";
    /*global document, window, console, XMLHttpRequest, JSON, alert, $ */
    /*jslint todo: true */

    return {

        /**
         * Mark a alert as accepted
         *
         * @param id
         * @returns {boolean}
         */
        validate: function (id) {
            var client, alertMessage;

            client = new XMLHttpRequest();
            client.open('GET', 'alert/accept/'.concat(id), true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                alertMessage = $("#alert-".concat(id));
                if (client.readyState === 4 && client.status === 200) {
                    alert('Pensez à refuser le commentaire');
                    alertMessage.slideUp();
                } else if (client.readyState === 4 && client.status === 500) {
                    console.log("Error : ".concat(client.responseText));
                    alertMessage.slideUp(function () {
                        alertMessage.slideDown();
                    });
                }
            };
            return false;
        },

        reject: function (id) {
            var client, alertMessage;

            client = new XMLHttpRequest();
            client.open('GET', 'alert/reject/'.concat(id), true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                alertMessage = $("#alert-".concat(id));
                if (client.readyState === 4 && client.status === 200) {
                    alertMessage.slideUp();
                } else if (client.readyState === 4 && client.status === 500) {
                    console.log("Error : ".concat(client.responseText));
                    alertMessage.slideUp(function () {
                        alertMessage.slideDown();
                    });
                }
            };
            return false;
        },

        /**
         * Bind validate buttons
         */
        bindValidateButtons: function () {
            var
                i, f,
                buttons, button,
                $$ = this;

            buttons = document.querySelectorAll(".validate-alert");

            f = function () {
                var id;

                id = this.getAttribute('id').substring("validate-alert-".length);
                $$.validate(id);

                return false;
            };
            for (i = 0; i < buttons.length; i += 1) {
                button = buttons[i];
                button.onclick = f;
            }
        },

        /**
         * Bind reject buttons
         */
        bindRejectButtons: function () {
            var
                i, f,
                buttons, button,
                $$ = this;

            buttons = document.querySelectorAll(".reject-alert");

            f = function () {
                var id, cause;

                id = this.getAttribute('id').substring("reject-alert-".length);
                cause = document.querySelector("#reject-alert-cause-".concat(id));
                $$.reject(id);

                return false;
            };
            for (i = 0; i < buttons.length; i += 1) {
                button = buttons[i];
                button.onclick = f;
            }

        },

        /**
         * execute all bind functions
         */
        bindAll: function () {
            this.bindValidateButtons();
            this.bindRejectButtons();
        }
    };
}());
