/*global document, window, console, XMLHttpRequest, JSON, US, jQuery, Highcharts, alert */

var
    yearChartsData = US.getDefaultYearChartsData();

US.drawChartsFromData = function (chartsData) {
    "use strict";

    var yearlyArray = US.getDataFromLabel(chartsData, 'yearlyComments');
    US.formatLineStats(yearlyArray, yearChartsData, false);

    jQuery(function () {
        jQuery(document).trigger("chartDataReady");
    });

};

US.onChartDataReady = function (chartInstanceSetting) {
    "use strict";
    yearChartsData.series[0].color = '#428bca';
    yearChartsData.series[1].color = '#47a447';
    yearChartsData.series[2].color = '#d2322d';
    yearChartsData.series[3].color = '#ed9c28';

    jQuery('#commentsStatContent').highcharts({
        title: { text: 'Commentaires des 12 derniers Mois' },
        //  jours du mois
        xAxis: yearChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'Commentaires' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: yearChartsData.series
    });


}
