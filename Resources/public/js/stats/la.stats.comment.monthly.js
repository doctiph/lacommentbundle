/*global document, window, console, XMLHttpRequest, JSON, US, jQuery, Highcharts, alert */

var
    monthChartsData = US.getDefaultMonthChartsData();

US.drawChartsFromData = function (chartsData) {
    "use strict";

    var monthlyArray;

    monthlyArray = US.getDataFromLabel(chartsData, 'monthlyComments');
    US.formatLineStats(monthlyArray, monthChartsData, true);

    jQuery(function () {
        jQuery(document).trigger("chartDataReady");
    });

};

US.onChartDataReady = function (chartInstanceSetting) {
    "use strict";
    monthChartsData.series[0].color = '#428bca';
    monthChartsData.series[1].color = '#47a447';
    monthChartsData.series[2].color = '#d2322d';
    monthChartsData.series[3].color = '#ed9c28';

    jQuery('#commentsStatContent').highcharts({
        title: { text: 'Commentaires des 30 derniers jours' },
        //  jours du mois
        xAxis: monthChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'Commentaires' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: monthChartsData.series
    });


}
