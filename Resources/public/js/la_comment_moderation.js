var la_comment_moderation;

la_comment_moderation = (function () {
    "use strict";
    /*global document, window, console, XMLHttpRequest, JSON, alert, $ */
    /*jslint todo: true */

    return {

        /**
         * Mark a comment as validated
         *
         * @param id
         * @returns {boolean}
         */
        validate: function (id) {
            var client, comment;

            client = new XMLHttpRequest();
            client.open('GET', 'validate/'.concat(id), true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                comment = $("#comment-".concat(id));
                if (client.readyState === 4 && client.status === 200) {
                    comment.slideUp();
                } else if (client.readyState === 4 && client.status === 500) {
                    console.log("Error : ".concat(client.responseText));
                    comment.slideUp(function () {
                        comment.slideDown();
                    });
                }
            };
            return false;
        },

        reject: function (id, cause) {
            var client, comment;

            client = new XMLHttpRequest();
            client.open('GET', 'reject/'.concat(id, '/', encodeURI(cause)), true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                comment = $("#comment-".concat(id));
                if (client.readyState === 4 && client.status === 200) {
                    comment.slideUp();
                } else if (client.readyState === 4 && client.status === 500) {
                    console.log("Error : ".concat(client.responseText));
                    comment.slideUp(function () {
                        comment.slideDown();
                    });
                }
            };
            return false;
        },

        /**
         * Bind validate buttons
         */
        bindValidateButtons: function () {
            var
                i, f,
                buttons, button,
                $$ = this;

            buttons = document.querySelectorAll(".validate-comment");

            f = function () {
                var id;

                id = this.getAttribute('id').substring("validate-comment-".length);
                $$.validate(id);
                return false;
            };
            for (i = 0; i < buttons.length; i += 1) {
                button = buttons[i];
                button.onclick = f;
            }
        },

        /**
         * Bind reject buttons
         */
        bindRejectButtons: function () {
            var
                i, f,
                buttons, button,
                $$ = this;

            buttons = document.querySelectorAll(".reject-comment");

            f = function () {
                var id, cause;

                id = this.getAttribute('id').substring("reject-comment-".length);
                cause = document.querySelector("#reject-comment-cause-".concat(id));
                if (cause !== null) {
                    if (cause.value !== "null") {
                        $$.reject(id, cause.value);
                    } else {
                        alert("Veuillez choisir une raison.");
                    }
                }
                return false;
            };
            for (i = 0; i < buttons.length; i += 1) {
                button = buttons[i];
                button.onclick = f;
            }

        },

        /**
         * execute all bind functions
         */
        bindAll: function () {
            this.bindValidateButtons();
            this.bindRejectButtons();
            //this.bindBanButtons();
        }
    };
}());
