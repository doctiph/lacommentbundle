# DOCUMENTATION API COMMENTAIRES

Cette documentation n'est plus mise à jour. Merci de vous reporter au nouveau système de documentation :
__http://profile.votre-site.tld/api/doc/threads__

## 1.	je peux saisir un commentaire en mode anonyme (TRANS-142)

    [POST] api/threads/{id-thread}/comments
        body: {texte-du-commentaire}
        headers: {headers}

## 2.	je peux saisir un commentaire en mode connecté (TRANS-143)

    [POST] api/threads/{id-thread}/comments
        body: {texte-du-commentaire}
        headers: {headers}
        userId: {userId}
        username: {username}
        avatar: {relative/path/to/avatar}
        email: {email}

## 3.	je peux supprimer un commentaire en mode connecté (TRANS-144)

    4 états possibles pour les commentaires :

    + VISIBLE : 0
    + DELETED : 1
    + SPAM    : 2
    + PENDING : 3

    [PATCH] api/threads/{id-thread}/comments/{id-comment}/state
        state: {state}
        userId: {userId}

## 4.	je peux répondre a un commentaire en mode anonyme (TRANS-148)

    Identique au 1 avec paramètre parentId dans url :

    [POST] api/threads/{id-thread}/comments?parentId={id_comment}
        body: {texte-du-commentaire}
        headers: {headers}

## 5.	je peux répondre a un commentaire en mode connecté (TRANS-149)

    Identique au 2 avec paramètre parentId dans url :

    [POST] api/threads/{id-thread}/comments?parentId={id_comment}
        body: {texte-du-commentaire}
        headers: {headers}
        userId: {userId}
        username: {username}
        avatar: {relative/path/to/avatar}
        email: {email}

## 6.	je peux ajouter une note à un commentaire (TRANS-145)

    Identique au post d'un commentaire, on rajoute simplement une note :

    [POST] api/threads/{id-thread}/comments
        (...)
        rating: {int-1-to-5}

## 7.	je peux "liker" un commentaire (TRANS-147)

    like :   [PATCH] api/threads/{id-thread}/comments/{id-comment}/upvote
    et
    unlike : [PATCH] api/threads/{id-thread}/comments/{id-comment}/downvote

## 8.	je peux modifier un commentaire en mode connecté (TRANS-146)

    [PUT] api/threads/{id-thread}/comments/{id-comment}
        body; {texte-du-commentaire}
        userId: {userId}

## 9.	je peux être averti par mail d'une réponse à un commentaire (TRANS-150)

**@TODO**

## 10.	Je peux trier les commentaires par date de création (TRANS-151)

    Du plus vieux au plus récent :
        [GET] api/treads/{id-thread}/comments?sorter=date_asc
    Du plus récent au plus vieux :
        [GET] api/treads/{id-thread}/comments?sorter=date_desc

## 11.	Je peux blacklister une IP (TRANS-154)

    Cf. Netino Moderation

## 12.	Je peux trier les commentaires par note (TRANS-153)

    Du plus mauvais au meilleur :
        [GET] api/treads/{id-thread}/comments?sorter=vote_asc
    Du meilleur au plus mauvais :
        [GET] api/treads/{id-thread}/comments?sorter=vote_desc

## 13.	je peux trier les commentaires par popularité (likes) (TRANS-152)

    Du plus mauvais au meilleur :
        [GET] api/treads/{id-thread}/comments?sorter=score_asc
    Du meilleur au plus mauvais :
        [GET] api/treads/{id-thread}/comments?sorter=score_desc

## 14.	Un commentaire est une chaîne de caractères alphanumériques saisie par un internaute (TRANS-141)

    OK
