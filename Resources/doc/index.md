LaCommentBundle
============

# Prerequis

LaApiBundle (et ses prérequis)
LaAdminBundle (et ses prérequis)
LaStatsBundle (et ses prérequis)

# Installation

### Nommage
Pour la documentation nous utiliserons le site www.elle.fr avec l'identification sur le domaine identification.elle.fr


# composer.json

    "repositories": [
        ....
        {
            "type": "git",
            "url": "git@git-pool01.in.ladtech.fr:LaCommentBundle"
        }
    ],


 "require": {
        ....
        "la/commentbundle": "dev-master"
    },



# RegisterBundle
Enregistrer les bundles dans app/AppKernel.php si ce n'est pas déjà fait.

    public function registerBundles()
    {
        $bundles = array(
            // ....
            new La\CommentBundle\LaCommentBundle(),
            new \Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            // ....
        );
    }

# Import des fichiers de routing

### app/config/routing.yml

    la_comment:
        resource: "@LaCommentBundle/Resources/config/routing.yml"
        prefix:   /
        defaults: { _format: json }

    ## Moderation
    la_comment_admin:
        resource: "@LaCommentBundle/Resources/config/admin.yml"
        prefix:   /admin


# Configuration


## Paramétrage du CommentBundle :

### app/config/la_config.yml :

    la_comment:
        db_driver: orm
        #   Les paramètres ci-dessous sont optionnels et ont une valeur par défaut si non-spécifiés:
        moderation:
            a_priori_moderation;    false   #   Modération à priori, ou à posteriori (par defaut à postériori)
            cascade_on_moderation:  true    #   La modération d'un message se répercute sur ses enfants (par défaut oui), ou non.
        allow_replies_on_reviews: false     #   Permettre des réponses aux commentaires sur les fils de type "review" (par défaut false)
        replies_sorter_alias: null          #   Permet de spécifier un tri différent pour les réponses (par défaut, c'est le même que les commentaires de niveau zéro)

# Suite de la mise en place des commentaires

Vous avez terminé de configurer le bundle gérant le webservice REST de commentaires.
Il faut désormais mettre en place un bundle de "front" qui communiquera avec le bundle que vous venez d'installer et gérera l'affichage des commentaires sur votre site.
Pour ce faire, nous vous conseillons d'utiliser le __CommentFrontBundle__.
Sinon, vous pouvez également interroger vous même le webservice en vous basant sur la documentation :
__http://profile.votre-site.tld/api/doc/threads__
