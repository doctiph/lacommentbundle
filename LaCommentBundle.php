<?php



namespace La\CommentBundle;

use La\CommentBundle\DependencyInjection\Compiler\SortingPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class LaCommentBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SortingPass());
    }
}
