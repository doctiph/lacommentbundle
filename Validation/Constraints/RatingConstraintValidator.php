<?php
namespace La\CommentBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use La\CommentBundle\Model\Thread;
use La\CommentBundle\Model\Comment;

class RatingConstraintValidator extends ConstraintValidator
{

    public function validate($comment, Constraint $constraint)
    {
        /** @var Thread $thread */
        $thread = $comment->getThread();
        $type = $thread->getType();
        $rating = $comment->getRating();

        $errorPath = $constraint->errorPath;

        /** @var Comment $comment */
        if (!in_array($rating, array(1, 2, 3, 4, 5))) {
            if ($type === $thread::REVIEW_TYPE && $comment->getDepth() == 0) {
                if(is_null($rating)){
                    $this->context->addViolationAt($errorPath, $constraint->message['blank']);
                }else{
                    $this->context->addViolationAt($errorPath, $constraint->message['outofbounds']);
                }
            }else{ // Si ce n'est pas une review, le rating n'est pas pris en compte, on set simplement la valeur par défaut.
                $comment->setRating(0);
            }
        }

    }
}