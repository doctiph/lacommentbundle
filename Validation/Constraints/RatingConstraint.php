<?php

namespace La\CommentBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class RatingConstraint extends Constraint
{

    public $errorPath = 'rating';

    public $message = array(
        'outofbounds' => 'la_comment.comment.rating.outofbounds',
        'blank' => 'la_comment.comment.rating.blank',
    );

    // the target is the class and not the property since we need to check thread type
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}