<?php



namespace La\CommentBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use InvalidArgumentException;

/**
 * Registers Sorting implementations.
 */
class SortingPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('la_comment.sorting_factory')) {
            return;
        }

        $sorters = array();
        foreach ($container->findTaggedServiceIds('la_comment.sorter') as $id => $tags) {
            foreach ($tags as $tag) {
                if (empty($tag['alias'])) {
                    throw new InvalidArgumentException('The Sorter must have an alias');
                }

                $sorters[$tag['alias']] = new Reference($id);
            }
        }

        $container->getDefinition('la_comment.sorting_factory')->replaceArgument(0, $sorters);
    }
}
