<?php



namespace La\CommentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Configures the DI container for CommentBundle.
 *
 */
class LaCommentExtension extends Extension
{
    /**
     * Loads and processes configuration to configure the Container.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \InvalidArgumentException
     */

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load('api.xml');
        $loader->load('cache.xml');
        $loader->load('twig.xml');
        $loader->load('admin.xml');
        $loader->load('stats.xml');

        if (!in_array(strtolower($config['db_driver']), array('mongodb', 'orm'))) {
            throw new \InvalidArgumentException(sprintf('Invalid db driver "%s".', $config['db_driver']));
        }
        $loader->load(sprintf('%s.xml', $config['db_driver']));

        foreach (array('events', 'form', 'sorting') as $basename) {
            $loader->load(sprintf('%s.xml', $basename));
        }

        $container->setParameter('la_comment.cache.thread_cache_ttl', $config['cache']['thread_ttl']);

        $container->setParameter('la_comment.moderation.a_priori', $config['moderation']['a_priori_moderation']);
        $container->setParameter('la_comment.moderation.cascade', $config['moderation']['cascade_on_moderation']);
        $container->setParameter('la_comment.moderation.cache_default_ttl', $config['moderation']['cache_default_ttl']);

        $container->setParameter('la_comment.config.allow_replies_on_reviews', $config['allow_replies_on_reviews']);
        $container->setParameter('la_comment.config.replies_sorter_alias', $config['replies_sorter_alias']);

        $container->setParameter('la_comment.template.engine', $config['template']['engine']);

        $container->setParameter('la_comment.model.comment.class', $config['class']['model']['comment']);
        $container->setParameter('la_comment.model.thread.class', $config['class']['model']['thread']);

        if (array_key_exists('vote', $config['class']['model'])) {
            $container->setParameter('la_comment.model.vote.class', $config['class']['model']['vote']);
        }

        $container->setParameter('la_comment.model_manager_name', $config['model_manager_name']);

        // handle the MongoDB document manager name in a specific way as it does not have a registry to make it easy
        // TODO: change it if https://github.com/symfony/DoctrineMongoDBBundle/pull/31 is merged
        if ('mongodb' === $config['db_driver']) {
            if (null === $config['model_manager_name']) {
                $container->setAlias('la_comment.document_manager', new Alias('doctrine.odm.mongodb.document_manager', false));
            } else {
                $container->setAlias('la_comment.document_manager', new Alias(sprintf('doctrine.odm.%s_mongodb.document_manager', $config['model_manager_name']), false));
            }
        }

        $container->setParameter('la_comment.form.comment.type', $config['form']['comment']['type']);
        $container->setParameter('la_comment.form.comment.name', $config['form']['comment']['name']);

        $container->setParameter('la_comment.form.thread.type', $config['form']['thread']['type']);
        $container->setParameter('la_comment.form.thread.name', $config['form']['thread']['name']);

        $container->setParameter('la_comment.form.commentable_thread.type', $config['form']['commentable_thread']['type']);
        $container->setParameter('la_comment.form.commentable_thread.name', $config['form']['commentable_thread']['name']);

        $container->setParameter('la_comment.form.enabled_thread.type', $config['form']['enabled_thread']['type']);
        $container->setParameter('la_comment.form.enabled_thread.name', $config['form']['enabled_thread']['name']);

        $container->setParameter('la_comment.form.delete_comment.type', $config['form']['delete_comment']['type']);
        $container->setParameter('la_comment.form.delete_comment.name', $config['form']['delete_comment']['name']);

        $container->setParameter('la_comment.form.vote.type', $config['form']['vote']['type']);
        $container->setParameter('la_comment.form.vote.name', $config['form']['vote']['name']);

        $container->setParameter('la_comment.sorting_factory.default_sorter', $config['service']['sorting']['default']);

        $container->setAlias('la_comment.form_factory.comment', $config['service']['form_factory']['comment']);
        $container->setAlias('la_comment.form_factory.commentable_thread', $config['service']['form_factory']['commentable_thread']);
        $container->setAlias('la_comment.form_factory.enabled_thread', $config['service']['form_factory']['enabled_thread']);
        $container->setAlias('la_comment.form_factory.delete_comment', $config['service']['form_factory']['delete_comment']);
        $container->setAlias('la_comment.form_factory.thread', $config['service']['form_factory']['thread']);
        $container->setAlias('la_comment.form_factory.vote', $config['service']['form_factory']['vote']);

        if (isset($config['service']['spam_detection'])) {
            $loader->load('spam_detection.xml');
            $container->setAlias('la_comment.spam_detection.comment', $config['service']['spam_detection']['comment']);
        }

        $container->setAlias('la_comment.manager.thread', $config['service']['manager']['thread']);
        $container->setAlias('la_comment.manager.comment', $config['service']['manager']['comment']);
        $container->setAlias('la_comment.manager.vote', $config['service']['manager']['vote']);
    }


}
