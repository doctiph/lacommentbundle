<?php



namespace La\CommentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This class contains the configuration information for the bundle
 *
 * This information is solely responsible for how the different configuration
 * sections are normalized, and merged.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree.
     *
     * @return NodeInterface
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('la_comment')
            ->children()

                ->arrayNode('moderation')->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('a_priori_moderation')->defaultValue(false)->end()
                        ->booleanNode('cascade_on_moderation')->defaultValue(true)->end()
                        ->scalarNode('cache_default_ttl')->defaultValue('300')->end()
                    ->end()
                ->end()

                ->booleanNode('allow_replies_on_reviews')->defaultValue(false)->end()
                ->scalarNode('replies_sorter_alias')->defaultValue(null)->end()

                ->arrayNode('cache')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('thread_ttl')->defaultValue(3600)->end()
                    ->end()
                ->end()

                ->scalarNode('db_driver')->cannotBeOverwritten()->isRequired()->end()
                ->scalarNode('model_manager_name')->defaultNull()->end()

                ->arrayNode('form')->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('comment')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_comment')->end()
                                ->scalarNode('name')->defaultValue('la_comment_comment')->end()
                            ->end()
                        ->end()
                        ->arrayNode('thread')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_thread')->end()
                                ->scalarNode('name')->defaultValue('la_comment_thread')->end()
                            ->end()
                        ->end()
                        ->arrayNode('commentable_thread')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_commentable_thread')->end()
                                ->scalarNode('name')->defaultValue('la_comment_commentable_thread')->end()
                            ->end()
                        ->end()
                        ->arrayNode('enabled_thread')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_enabled_thread')->end()
                                ->scalarNode('name')->defaultValue('la_comment_enabled_thread')->end()
                            ->end()
                        ->end()
                        ->arrayNode('delete_comment')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_delete_comment')->end()
                                ->scalarNode('name')->defaultValue('la_comment_delete_comment')->end()
                            ->end()
                        ->end()
                        ->arrayNode('vote')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('type')->defaultValue('la_comment_vote')->end()
                                ->scalarNode('name')->defaultValue('la_comment_vote')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('class')->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('model')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('comment')->defaultValue('La\\CommentBundle\\Entity\\Comment')->end()
                                ->scalarNode('thread')->defaultValue('La\\CommentBundle\\Entity\\Thread')->end()
                                ->scalarNode('vote')->defaultValue('La\\CommentBundle\\Entity\\Vote')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('template')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('engine')->defaultValue('twig')->end()
                    ->end()
                ->end()

                ->arrayNode('service')->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('manager')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('comment')->cannotBeEmpty()->defaultValue('la_comment.manager.comment.default')->end()
                                ->scalarNode('thread')->cannotBeEmpty()->defaultValue('la_comment.manager.thread.default')->end()
                                ->scalarNode('vote')->cannotBeEmpty()->defaultValue('la_comment.manager.vote.default')->end()
                            ->end()
                        ->end()
                        ->arrayNode('form_factory')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('comment')->cannotBeEmpty()->defaultValue('la_comment.form_factory.comment.default')->end()
                                ->scalarNode('commentable_thread')->cannotBeEmpty()->defaultValue('la_comment.form_factory.commentable_thread.default')->end()
                                ->scalarNode('enabled_thread')->cannotBeEmpty()->defaultValue('la_comment.form_factory.enabled_thread.default')->end()
                                ->scalarNode('delete_comment')->cannotBeEmpty()->defaultValue('la_comment.form_factory.delete_comment.default')->end()
                                ->scalarNode('thread')->cannotBeEmpty()->defaultValue('la_comment.form_factory.thread.default')->end()
                                ->scalarNode('vote')->cannotBeEmpty()->defaultValue('la_comment.form_factory.vote.default')->end()
                            ->end()
                        ->end()
                        ->arrayNode('spam_detection')
                            ->children()
                                ->scalarNode('comment')->end()
                            ->end()
                        ->end()
                        ->arrayNode('sorting')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('default')->cannotBeEmpty()->defaultValue('date_desc')->end()
                            ->end()
                        ->end()
                        ->scalarNode('markup')->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
