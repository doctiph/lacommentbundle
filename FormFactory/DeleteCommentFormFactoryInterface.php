<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * DeleteComment form creator
 */
interface DeleteCommentFormFactoryInterface
{
    /**
     * Creates a delete comment form
     *
     * @return FormInterface
     */
    public function createForm();
}
