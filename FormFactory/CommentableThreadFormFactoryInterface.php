<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * CommentableThread form creator
 */
interface CommentableThreadFormFactoryInterface
{
    /**
     * Creates a open thread form
     *
     * @return FormInterface
     */
    public function createForm();
}
