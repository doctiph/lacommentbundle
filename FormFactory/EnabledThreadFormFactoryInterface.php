<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * EnabledThread form creator
 */
interface EnabledThreadFormFactoryInterface
{
    /**
     * Creates a open thread form
     *
     * @return FormInterface
     */
    public function createForm();
}
