<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * Vote form creator
 *
 */
interface VoteFormFactoryInterface
{
    /**
     * Creates a comment form
     *
     * @return FormInterface
     */
    public function createForm();
}
