<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * Thread form creator
 */
interface ThreadFormFactoryInterface
{
    /**
     * Creates a thread form
     *
     * @return FormInterface
     */
    public function createForm();
}
