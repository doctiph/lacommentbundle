<?php



namespace La\CommentBundle\FormFactory;

use Symfony\Component\Form\FormInterface;

/**
 * Comment form creator
 *
 */
interface CommentFormFactoryInterface
{
    /**
     * Creates a comment form
     *
     * @return FormInterface
     */
    public function createForm();
}
