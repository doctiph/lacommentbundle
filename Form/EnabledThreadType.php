<?php



namespace La\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnabledThreadType extends AbstractType
{
    private $threadClass;

    public function __construct($threadClass)
    {
        $this->threadClass = $threadClass;
    }

    /**
     * Configures a form to close a thread.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('isEnabled', 'hidden', array('property_path' => 'enabled'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => $this->threadClass,
        ));
    }

    public function getName()
    {
        return "la_comment_enabled_thread";
    }
}
