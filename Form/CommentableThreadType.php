<?php



namespace La\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentableThreadType extends AbstractType
{
    private $threadClass;

    public function __construct($threadClass)
    {
        $this->threadClass = $threadClass;
    }

    /**
     * Configures a form to close a thread.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('isCommentable', 'hidden', array('property_path' => 'commentable'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => $this->threadClass,
        ));
    }

    public function getName()
    {
        return "la_comment_commentable_thread";
    }
}
