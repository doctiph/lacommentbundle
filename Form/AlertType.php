<?php

namespace La\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use La\CommentBundle\Form\DataTransformer\CommentToIdTransformer;

class AlertType extends AbstractType
{
    /**
     * Configures a Comment form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $commentManager = $options['commentManager'];

        $builder->add('email', 'email', array('label' => 'alert.label.email'));
        /** @todo valider l'email */
        $builder->add('description', 'textarea', array('label' => 'alert.label.description'));
        $builder->add('captcha', 'genemu_captcha', array('mapped' => false, 'label' => 'alert.label.captcha'));
        $builder->add($builder->create('comment', 'hidden')->addViewTransformer(new CommentToIdTransformer($commentManager)));
        $builder->add('submit', 'submit', array('label' => 'alert.label.submit'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => "La\\CommentBundle\\Entity\\Alert",
        ));
        // A QUOI CA SERT CE TRUC ???
        $resolver->setRequired(array(
            'commentManager',
        ));

        $resolver->setAllowedTypes(array(
            'commentManager' => 'La\CommentBundle\Model\CommentManager',
        ));
    }

    public function getName()
    {
        return "la_comment_alert";
    }
}
