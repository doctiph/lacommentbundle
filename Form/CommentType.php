<?php



namespace La\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType
{
    private $commentClass;

    public function __construct($commentClass)
    {
        $this->commentClass = $commentClass;
    }

    /**
     * Configures a Comment form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', array('required' => false));
        $builder->add('body', 'textarea');
        $builder->add('headers', 'textarea');
        $builder->add('cookie', 'hidden');

        $builder->add('rating', 'text', array('required' => false));

        // comment author
        $builder->add('username', 'text', array('required' => false));
        $builder->add('email', 'text', array('required' => false));
        $builder->add('userId', 'text', array('required' => false));
        $builder->add('avatar', 'text', array('required' => false));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => $this->commentClass,
        ));
    }

    public function getName()
    {
        return "la_comment_comment";
    }
}
