<?php

namespace La\CommentBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use La\CommentBundle\Model\CommentManager;
use La\CommentBundle\Entity\Comment;

class CommentToIdTransformer implements DataTransformerInterface
{
    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * @param CommentManager $commentManager
     */
    public function __construct(CommentManager $commentManager)
    {
        $this->commentManager = $commentManager;
    }

    /**
     * Transforms a comment to a string (its id).
     *
     * @param mixed $comment
     * @return string
     */
    public function transform($comment)
    {
        if (null === $comment) {
            return "";
        }

        return $comment->getId();
    }

    /**
     * Transforms a string (id) to a comment.
     *
     * @param  string $id
     * @return Comment|null
     * @throws TransformationFailedException if comment is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $comment = $this->commentManager->findCommentById($id);

        if (null === $comment) {
            throw new TransformationFailedException(sprintf(
                'No comment found with id %s',
                $id
            ));
        }

        return $comment;
    }
}