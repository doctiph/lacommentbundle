<?php



namespace La\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ThreadType extends AbstractType
{
    private $threadClass;

    public function __construct($threadClass)
    {
        $this->threadClass = $threadClass;
    }

    /**
     * Configures a Thread form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'text');
        $builder->add('permalink', 'textarea');
        $builder->add('type', 'text');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => $this->threadClass,
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return "la_comment_thread";
    }
}
